package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.ProdukModel;

public interface ProdukService {

	public void create(ProdukModel produkModel) throws Exception;

	public List<ProdukModel> list() throws Exception;

	public List<ProdukModel> selectKodeOrNama(String kodeProduk, String namaProduk);

	public List<ProdukModel> cariProduk(String keywordCari, String tipeCari);

	public ProdukModel cariKode(String kodeProduk) throws Exception;

	public void update(ProdukModel produkModel) throws Exception;

	public void delete(ProdukModel produkModel) throws Exception;
	
	public List<ProdukModel> listIsNotDelete() throws Exception;

	public List<ProdukModel> cariNamaProduk(String namaProduk) throws Exception;
	
	public Integer sequenceValueProduk() throws Exception;
}
