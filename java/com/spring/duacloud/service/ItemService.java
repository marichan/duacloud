package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.ItemModel; 

public interface ItemService {
	public void create(ItemModel itemModel) throws Exception;
	
	public List<ItemModel> list() throws Exception;
	
	public List<ItemModel> selectKodeOrNama(String kodeItem, String namaItem); 
	
	public List<ItemModel> cariItem(String keywordCari, String tipeCari);
	
	public ItemModel cariKode(String kodeItem) throws Exception;
	
	public void update(ItemModel itemModel) throws Exception;
	
	public void delete(ItemModel itemModel) throws Exception;
	
	public List<ItemModel> listIsNotDelete() throws Exception;
	
	public List<ItemModel> cariNamaItem(String namaItem) throws Exception;
	
	public Integer sequenceValueItem() throws Exception;
}
