package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.KategoriModel; 

public interface KategoriService {
	
	/*transaksi simpan data*/
	public void create(KategoriModel kategoriModel) throws Exception;
	
	/*transaksi list data*/
	public List<KategoriModel> list() throws Exception;
	
	/*select * from M_KATEGORI
	public List<KategoriModel> selectBintang();*/
	
	/*patternnya
	public Output namaTrx(Input);*/
	
	/*select * from M_KATEGORI where KODE_KATEGORI = 'kat001';
	public KategoriModel selectKode(String kodeKategori);*/
	
	/*select * from M_KATEORI where KODE_KATEGORI != 'kat001' or NAMA_KATEGORI != 'namakat002'; */
	public List<KategoriModel> selectKodeOrNama(String kodeKategori, String namaKategori); 
	
	public List<KategoriModel> cariKategori(String keywordCari, String tipeCari);
	
	public KategoriModel cariKode(String kodeKategori) throws Exception;
	
	public void update(KategoriModel kategoriModel) throws Exception;
	
	public void delete(KategoriModel kategoriModel) throws Exception;
	
	public List<KategoriModel> listIsNotDelete() throws Exception;
	
	public List<KategoriModel> cariNamaKategori(String namaKategori) throws Exception;
	
	public Integer sequenceValueKategori() throws Exception;
	
}
