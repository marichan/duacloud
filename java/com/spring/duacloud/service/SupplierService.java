package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.SupplierModel;

public interface SupplierService {

	public void create(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> list() throws Exception;
	
	public List<SupplierModel> selectKodeOrNama(String kodeKategori, String namaKategori); 
	
	public List<SupplierModel> cariSupplier(String keywordCari, String tipeCari);
	
	public SupplierModel cariKode(String kodeSupplier) throws Exception;
	
	public void update(SupplierModel supplierModel) throws Exception;
	
	public void delete(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> listIsNotDelete() throws Exception;
	
	//patternnya: public output namaMethod(input)
	
	public List<SupplierModel> cariNamaSupplier(String namaSupplier) throws Exception;
	
	public Integer sequenceValueSupplier() throws Exception;
}
