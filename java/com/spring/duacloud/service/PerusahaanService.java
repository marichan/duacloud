package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.PerusahaanModel;

public interface PerusahaanService {
	public void create(PerusahaanModel perusahaanModel) throws Exception;

	public List<PerusahaanModel> list() throws Exception;

	public List<PerusahaanModel> selectKodeOrNama(String kodePerusahaan, String namaPerusahaan);

	public List<PerusahaanModel> cariPerusahaan(String keywordCari, String tipeCari);

	public PerusahaanModel cariKode(String kodePerusahaan) throws Exception;

	public void update(PerusahaanModel perusahaanModel) throws Exception;

	public void delete(PerusahaanModel perusahaanModel) throws Exception;
}
