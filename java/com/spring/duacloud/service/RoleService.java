package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.RoleModel;

public interface RoleService {
	public void create(RoleModel roleModel) throws Exception;

	public List<RoleModel> list() throws Exception;

	public List<RoleModel> selectKodeOrNama(String kodeRole, String namaRole);

	public List<RoleModel> cariRole(String keywordCari, String tipeCari);

	public RoleModel cariKode(String kodeRole) throws Exception;

	public void update(RoleModel roleModel) throws Exception;

	public void delete(RoleModel roleModel) throws Exception;
}
