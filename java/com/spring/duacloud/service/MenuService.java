package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.MenuModel;

public interface MenuService {
	public void create(MenuModel menuModel) throws Exception;

	public List<MenuModel> list() throws Exception;

	public List<MenuModel> selectKodeOrNama(String kodeMenu, String namaMenu);

	public List<MenuModel> cariMenu(String keywordCari, String tipeCari);

	public MenuModel cariKode(String kodeMenu) throws Exception;

	public void update(MenuModel menuModel) throws Exception;

	public void delete(MenuModel menuModel) throws Exception;
	
	public List<MenuModel> getAllMenuTreeByRole(String kodeRole) throws Exception;
}
