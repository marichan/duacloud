package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.UserModel;

public interface UserService {
	public void create(UserModel userModel) throws Exception;

	public List<UserModel> list() throws Exception;

	public List<UserModel> selectKodeOrNama(String kodeUser, String namaUser);

	public List<UserModel> cariUser(String keywordCari, String tipeCari);

	public UserModel cariKode(String kodeUser) throws Exception;

	public void update(UserModel userModel) throws Exception;

	public void delete(UserModel userModel) throws Exception;
	
	public UserModel searchByUsernamePassword(String username, String password) throws Exception;
}
