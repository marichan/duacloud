package com.spring.duacloud.service;

import java.util.List;

import com.spring.duacloud.model.KaryawanModel;

public interface KaryawanService {

	public void create(KaryawanModel karyawanModel) throws Exception;

	public List<KaryawanModel> list() throws Exception;

	public List<KaryawanModel> selectKodeOrNama(String kodeKaryawan, String namaKaryawan);

	public List<KaryawanModel> cariKaryawan(String keywordCari, String tipeCari);

	public KaryawanModel cariKode(String kodeKaryawan) throws Exception;

	public void update(KaryawanModel karyawanModel) throws Exception;

	public void delete(KaryawanModel karyawanModel) throws Exception;
}
