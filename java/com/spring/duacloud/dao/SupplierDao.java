package com.spring.duacloud.dao;

import java.util.List;

import com.spring.duacloud.model.SupplierModel;

public interface SupplierDao {
	
	public void create(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> list() throws Exception;
	
	public List<SupplierModel> selectKodeOrNama(String kodeKategori, String namaKategori); 
	
	public List<SupplierModel> cariKategori(String keywordCari, String tipeCari);
	
	public SupplierModel cariKode(String kodeSupplier) throws Exception;
	
	public void update(SupplierModel supplierModel) throws Exception;
	
	public void delete(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> listIsNotDelete() throws Exception;
	
	public List<SupplierModel> cariNamaSupplier(String namaSupplier) throws Exception;
	
	public Integer sequenceValueSupplier() throws Exception;
}
