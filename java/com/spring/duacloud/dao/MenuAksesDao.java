package com.spring.duacloud.dao;

import java.util.List;

import com.spring.duacloud.model.MenuAksesModel;

public interface MenuAksesDao {
	public void create(MenuAksesModel menuAksesModel) throws Exception;

	public List<MenuAksesModel> list() throws Exception;

	public List<MenuAksesModel> selectKodeOrNama(String kodeMenuAkses, String namaMenuAkses);

	public List<MenuAksesModel> cariMenuAkses(String keywordCari, String tipeCari);

	public MenuAksesModel cariKode(String kodeMenuAkses) throws Exception;

	public void update(MenuAksesModel menuAksesModel) throws Exception;

	public void delete(MenuAksesModel menuAksesModel) throws Exception;
}
