package com.spring.duacloud.dao;

import java.util.List;

import com.spring.duacloud.model.DepartemenModel;

public interface DepartemenDao {
	public void create(DepartemenModel departemenModel) throws Exception;

	public List<DepartemenModel> list() throws Exception;

	public List<DepartemenModel> selectKodeOrNama(String kodeDepartemen, String namaDepartemen);

	public List<DepartemenModel> cariDepartemen(String keywordCari, String tipeCari);

	public DepartemenModel cariKode(String kodeDepartemen) throws Exception;

	public void update(DepartemenModel departemenModel) throws Exception;

	public void delete(DepartemenModel departemenModel) throws Exception;
}
