package com.spring.duacloud.dao;

import java.util.List;

import com.spring.duacloud.model.RoleModel;

public interface RoleDao {
	public void create(RoleModel roleModel) throws Exception;

	public List<RoleModel> list() throws Exception;

	public List<RoleModel> selectKodeOrNama(String kodeRole, String namaRole);

	public List<RoleModel> cariRole(String keywordCari, String tipeCari);

	public RoleModel cariKode(String kodeRole) throws Exception;

	public void update(RoleModel roleModel) throws Exception;

	public void delete(RoleModel roleModel) throws Exception;
}
