package com.spring.duacloud.dao;

import java.util.List;

import com.spring.duacloud.model.KategoriModel;

public interface KategoriDao {

	/*query simpan data*/
	public void create(KategoriModel kategoriModel) throws Exception;
	
	/*query list data*/
	public List<KategoriModel> list() throws Exception;
	
	public List<KategoriModel> selectKodeOrNama(String kodeKategori, String namaKategori); 
	
	public List<KategoriModel> cariKategori(String keywordCari, String tipeCari);
	
	public KategoriModel cariKode(String kodeKategori) throws Exception;
	
	public void update(KategoriModel kategoriModel) throws Exception;
	
	public void delete(KategoriModel kategoriModel) throws Exception;
	
	public List<KategoriModel> listIsNotDelete() throws Exception;
	
	public List<KategoriModel> cariNamaKategori(String namaKategori) throws Exception;
	
	public Integer sequenceValueKategori() throws Exception;
}
