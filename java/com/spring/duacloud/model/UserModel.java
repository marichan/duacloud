package com.spring.duacloud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="M_USER")
public class UserModel {
	private String kodeUser;
	private String username;
	private String password;
	
	
	// join table role
	private String kodeRole;
	private RoleModel roleModel;
	
	// join table karyawan
	private String kodeKaryawan;
	private KaryawanModel karyawanModel;
	private Integer enabled;
	
	
	@Id
	@Column(name="KODE_USER")
	public String getKodeUser() {
		return kodeUser;
	}
	public void setKodeUser(String kodeUser) {
		this.kodeUser = kodeUser;
	}
	
	@Column(name="USERNAME")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="KODE_ROLE")
	public String getKodeRole() {
		return kodeRole;
	}
	public void setKodeRole(String kodeRole) {
		this.kodeRole = kodeRole;
	}
	
	@ManyToOne
	@JoinColumn(name="KODE_ROLE", nullable=true, updatable=false, insertable=false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	
	@Column(name="KODE_KARYAWAN")
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	
	@ManyToOne
	@JoinColumn(name="KODE_KARYAWAN", nullable=true, updatable=false, insertable=false)
	public KaryawanModel getKaryawanModel() {
		return karyawanModel;
	}
	public void setKaryawanModel(KaryawanModel karyawanModel) {
		this.karyawanModel = karyawanModel;
	}
	
	@Column(name="ENABLED")
	public Integer getEnabled() {
		return enabled;
	}
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
}
