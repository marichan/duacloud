package com.spring.duacloud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="M_KARYAWAN")
public class KaryawanModel {
	private String kodeKaryawan;
	private String namaKaryawan;
	
	// join table departemen
	private String kodeDepartemen;
	private DepartemenModel departemenModel;
	
	// join table perusahaan
	private String kodePerusahaan;
	private PerusahaanModel perusahaanModel;
	
	@Id
	@Column(name="KODE_KARYAWAN")
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	
	@Column(name="NAMA_KARYAWAN")
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	
	@Column(name="KODE_DEPARTEMEN")
	public String getKodeDepartemen() {
		return kodeDepartemen;
	}
	public void setKodeDepartemen(String kodeDepartemen) {
		this.kodeDepartemen = kodeDepartemen;
	}
	
	@ManyToOne 
	@JoinColumn(name="KODE_DEPARTEMEN", nullable=true, updatable=false, insertable=false)
	public DepartemenModel getDepartemenModel() {
		return departemenModel;
	}
	public void setDepartemenModel(DepartemenModel departemenModel) {
		this.departemenModel = departemenModel;
	}
	
	@Column(name="KODE_PERUSAHAAN")
	public String getKodePerusahaan() {
		return kodePerusahaan;
	}
	public void setKodePerusahaan(String kodePerusahaan) {
		this.kodePerusahaan = kodePerusahaan;
	}
	
	@ManyToOne 
	@JoinColumn(name="KODE_PERUSAHAAN", nullable=true, updatable=false, insertable=false)
	public PerusahaanModel getPerusahaanModel() {
		return perusahaanModel;
	}
	public void setPerusahaanModel(PerusahaanModel perusahaanModel) {
		this.perusahaanModel = perusahaanModel;
	}
	
	
}
