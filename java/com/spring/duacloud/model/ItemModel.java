package com.spring.duacloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_ITEM")
public class ItemModel {
	
	private Integer idItem;
	
	private String kodeItem;
	private String namaItem;
	
	private Integer hargaJual;
	private String keteranganItem;
	
	// join table ke kategori
	//private String kodeKategori;
	private Integer idKategori;
	private KategoriModel kategoriModel;
	
	// join table ke supplier
	//private String kodeSupplier;
	private Integer idSupplier;
	private SupplierModel supplierModel;
	
	//set audit trail
	private String xCreatedByItem;
	private UserModel xCreatedByUserItem;
			
	private Date xCreatedDateItem;
		
	private String xUpdatedByItem;
	private UserModel xUpdatedByUserItem;
			
	private Date xUpdatedDateItem;
	private Integer xIsDeleteItem;
	
	@Id
	@Column(name="ID_ITEM")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_ITEM")
	@TableGenerator(name="M_ITEM", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME", pkColumnValue="M_ID_ITEM", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdItem() {
		return idItem;
	}
	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}
	
	@Column(name = "KODE_ITEM", nullable = true)
	public String getKodeItem() {
		return kodeItem;
	}
	public void setKodeItem(String kodeItem) {
		this.kodeItem = kodeItem;
	}
	
	@Column(name = "NAMA_ITEM", nullable = true)
	public String getNamaItem() {
		return namaItem;
	}
	public void setNamaItem(String namaItem) {
		this.namaItem = namaItem;
	}
	
	
	
	@Column(name = "HARGA_JUAL", nullable = true)
	public Integer getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(Integer hargaJual) {
		this.hargaJual = hargaJual;
	}
	
	@Column(name = "KETERANGAN_ITEM", nullable = true)
	public String getKeteranganItem() {
		return keteranganItem;
	}
	public void setKeteranganItem(String keteranganItem) {
		this.keteranganItem = keteranganItem;
	}
	
	@Column(name="ID_KATEGORI")
	public Integer getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(Integer idKategori) {
		this.idKategori = idKategori;
	}

	@ManyToOne 
	@JoinColumn(name="ID_KATEGORI", nullable=true, updatable=false, insertable=false)
	public KategoriModel getKategoriModel() {
		return kategoriModel;
	}
	public void setKategoriModel(KategoriModel kategoriModel) {
		this.kategoriModel = kategoriModel;
	}
	 
	@Column(name="ID_SUPPLIER")
	public Integer getIdSupplier() {
		return idSupplier;
	}
	public void setIdSupplier(Integer idSupplier) {
		this.idSupplier = idSupplier;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_SUPPLIER", nullable=true, updatable=false, insertable=false)
	public SupplierModel getSupplierModel() {
		return supplierModel;
	}
	public void setSupplierModel(SupplierModel supplierModel) {
		this.supplierModel = supplierModel;
	}
	
	@Column(name="X_CREATED_BY_ITEM")
	public String getxCreatedByItem() {
		return xCreatedByItem;
	}
	public void setxCreatedByItem(String xCreatedByItem) {
		this.xCreatedByItem = xCreatedByItem;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_CREATED_BY_ITEM", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedByUserItem() {
		return xCreatedByUserItem;
	}
	public void setxCreatedByUserItem(UserModel xCreatedByUserItem) {
		this.xCreatedByUserItem = xCreatedByUserItem;
	}
	
	@Column(name = "X_CREATED_DATE_ITEM")
	public Date getxCreatedDateItem() {
		return xCreatedDateItem;
	}
	public void setxCreatedDateItem(Date xCreatedDateItem) {
		this.xCreatedDateItem = xCreatedDateItem;
	}
	
	@Column (name = "X_UPDATED_BY_ITEM")
	public String getxUpdatedByItem() {
		return xUpdatedByItem;
	}
	public void setxUpdatedByItem(String xUpdatedByItem) {
		this.xUpdatedByItem = xUpdatedByItem;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_UPDATED_BY_ITEM", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedByUserItem() {
		return xUpdatedByUserItem;
	}
	public void setxUpdatedByUserItem(UserModel xUpdatedByUserItem) {
		this.xUpdatedByUserItem = xUpdatedByUserItem;
	}
	
	@Column (name = "X_UPDATED_DATE_ITEM")
	public Date getxUpdatedDateItem() {
		return xUpdatedDateItem;
	}
	public void setxUpdatedDateItem(Date xUpdatedDateItem) {
		this.xUpdatedDateItem = xUpdatedDateItem;
	}
	
	@Column (name = "X_IS_DELETE_ITEM")
	public Integer getxIsDeleteItem() {
		return xIsDeleteItem;
	}
	public void setxIsDeleteItem(Integer xIsDeleteItem) {
		this.xIsDeleteItem = xIsDeleteItem;
	}
	
}
