package com.spring.duacloud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="M_PERUSAHAAN")
public class PerusahaanModel {
	private String kodePerusahaan;
	private String namaPerusahaan;
	private String telpPerusahaan;
	
	@Id
	@Column(name="KODE_PERUSAHAAN")
	public String getKodePerusahaan() {
		return kodePerusahaan;
	}
	public void setKodePerusahaan(String kodePerusahaan) {
		this.kodePerusahaan = kodePerusahaan;
	}
	
	@Column(name="NAMA_PERUSAHAAN")
	public String getNamaPerusahaan() {
		return namaPerusahaan;
	}
	public void setNamaPerusahaan(String namaPerusahaan) {
		this.namaPerusahaan = namaPerusahaan;
	}
	
	@Column(name="TELP_PERUSAHAAN")
	public String getTelpPerusahaan() {
		return telpPerusahaan;
	}
	public void setTelpPerusahaan(String telpPerusahaan) {
		this.telpPerusahaan = telpPerusahaan;
	}
	
	
	
}
