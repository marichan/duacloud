package com.spring.duacloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "MST_SUPPLIER")
public class SupplierModel {

	private Integer idSupplier; //PK dinamis
	
	private String kodeSupplier;
	private String namaSupplier;
	private Integer tipeSupplier;
	private String picSupplier;
	private Integer telpSupplier;
	private String emailSupplier;
	private String alamatSupplier;
	
	//Audit Trail 
	private String xCreatedBySupplier;
	private UserModel xCreatedByUserSupplier;
		
	private Date xCreatedDateSupplier;
		
	private String xUpdatedBySupplier;
	private UserModel xUpdatedByUserSupplier;
		
	private Date xUpdatedDateSupplier;
	private Integer xIsDeleteSupplier;
	//Audit trail
	
	@Id
	@Column(name="ID_SUPPLIER")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="MST_SUPPLIER")
	@TableGenerator(name="MST_SUPPLIER", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME", pkColumnValue="M_ID_SUPPLIER", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdSupplier() {
		return idSupplier;
	}
	public void setIdSupplier(Integer idSupplier) {
		this.idSupplier = idSupplier;
	}
	
	@Column (name = "KODE_SUPPLIER")
	public String getKodeSupplier() {
		return kodeSupplier;
	}
	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	
	@Column (name = "NAMA_SUPPLIER")
	public String getNamaSupplier() {
		return namaSupplier;
	}
	public void setNamaSupplier(String namaSupplier) {
		this.namaSupplier = namaSupplier;
	}
	
	@Column (name = "TIPE_SUPPLIER")
	public Integer getTipeSupplier() {
		return tipeSupplier;
	}
	public void setTipeSupplier(Integer tipeSupplier) {
		this.tipeSupplier = tipeSupplier;
	}
	
	@Column (name = "PIC_SUPPLIER")
	public String getPicSupplier() {
		return picSupplier;
	}
	public void setPicSupplier(String picSupplier) {
		this.picSupplier = picSupplier;
	}
	
	@Column (name = "TELP_SUPPLIER")
	public Integer getTelpSupplier() {
		return telpSupplier;
	}
	public void setTelpSupplier(Integer telpSupplier) {
		this.telpSupplier = telpSupplier;
	}
	
	@Column (name = "EMAIL_SUPPLIER")
	public String getEmailSupplier() {
		return emailSupplier;
	}
	public void setEmailSupplier(String emailSupplier) {
		this.emailSupplier = emailSupplier;
	}
	
	@Column (name = "ALAMAT_SUPPLIER")
	public String getAlamatSupplier() {
		return alamatSupplier;
	}
	public void setAlamatSupplier(String alamatSupplier) {
		this.alamatSupplier = alamatSupplier;
	}
	
	@Column(name = "X_CREATED_BY_SUPPLIER")
	public String getxCreatedBySupplier() {
		return xCreatedBySupplier;
	}
	public void setxCreatedBySupplier(String xCreatedBySupplier) {
		this.xCreatedBySupplier = xCreatedBySupplier;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_CREATED_BY_SUPPLIER", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedByUserSupplier() {
		return xCreatedByUserSupplier;
	}
	public void setxCreatedByUserSupplier(UserModel xCreatedByUserSupplier) {
		this.xCreatedByUserSupplier = xCreatedByUserSupplier;
	}
	
	@Column(name = "X_CREATED_DATE_SUPPLIER")
	public Date getxCreatedDateSupplier() {
		return xCreatedDateSupplier;
	}
	public void setxCreatedDateSupplier(Date xCreatedDateSupplier) {
		this.xCreatedDateSupplier = xCreatedDateSupplier;
	}
	
	@Column (name = "X_UPDATED_BY_SUPPLIER")
	public String getxUpdatedBySupplier() {
		return xUpdatedBySupplier;
	}
	public void setxUpdatedBySupplier(String xUpdatedBySupplier) {
		this.xUpdatedBySupplier = xUpdatedBySupplier;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_UPDATED_BY_SUPPLIER", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedByUserSupplier() {
		return xUpdatedByUserSupplier;
	}
	public void setxUpdatedByUserSupplier(UserModel xUpdatedByUserSupplier) {
		this.xUpdatedByUserSupplier = xUpdatedByUserSupplier;
	}
	
	@Column (name = "X_UPDATED_DATE_SUPPLIER")
	public Date getxUpdatedDateSupplier() {
		return xUpdatedDateSupplier;
	}
	public void setxUpdatedDateSupplier(Date xUpdatedDateSupplier) {
		this.xUpdatedDateSupplier = xUpdatedDateSupplier;
	}
	
	@Column (name = "X_IS_DELETE_SUPPLIER")
	public Integer getxIsDeleteSupplier() {
		return xIsDeleteSupplier;
	}
	public void setxIsDeleteSupplier(Integer xIsDeleteSupplier) {
		this.xIsDeleteSupplier = xIsDeleteSupplier;
	}
	 
}
