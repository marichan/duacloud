package com.spring.duacloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_PRODUK")
public class ProdukModel {
	
	private Integer idProduk; //PK dinamis
	
	private String kodeProduk;
	private String namaProduk;
	private Integer hargaProduk;
	private Integer biayaProduk;
	private Integer hargaTotal;
	private String keteranganProduk;

	// join table ke kategori
	private Integer idKategori;
	private KategoriModel kategoriModel;

	// join table ke supplier
	private Integer idSupplier;
	private SupplierModel supplierModel;
	
	//set audit trail
	private String xCreatedByProduk;
	private UserModel xCreatedByUserProduk;
				
	private Date xCreatedDateProduk;
			
	private String xUpdatedByProduk;
	private UserModel xUpdatedByUserProduk;
				
	private Date xUpdatedDateProduk;
	private Integer xIsDeleteProduk;

	@Id
	@Column(name="ID_PRODUK")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_PRODUK")
	@TableGenerator(name="M_PRODUK", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME", pkColumnValue="M_ID_PRODUK", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdProduk() {
		return idProduk;
	}

	public void setIdProduk(Integer idProduk) {
		this.idProduk = idProduk;
	}

	@Column(name = "KODE_PRODUK", nullable = true)
	public String getKodeProduk() {
		return kodeProduk;
	}

	public void setKodeProduk(String kodeProduk) {
		this.kodeProduk = kodeProduk;
	}

	@Column(name = "NAMA_PRODUK", nullable = true)
	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}

	@Column(name = "HARGA_PRODUK", nullable = true)
	public Integer getHargaProduk() {
		return hargaProduk;
	}

	public void setHargaProduk(Integer hargaProduk) {
		this.hargaProduk = hargaProduk;
	}

	@Column(name = "BIAYA_PRODUK", nullable = true)
	public Integer getBiayaProduk() {
		return biayaProduk;
	}

	public void setBiayaProduk(Integer biayaProduk) {
		this.biayaProduk = biayaProduk;
	}

	@Column(name = "HARGA_TOTAL", nullable = true)
	public Integer getHargaTotal() {
		return hargaTotal;
	}

	public void setHargaTotal(Integer hargaTotal) {
		this.hargaTotal = hargaTotal;
	}

	@Column(name = "KETERANGAN_PRODUK", nullable = true)
	public String getKeteranganProduk() {
		return keteranganProduk;
	}

	public void setKeteranganProduk(String keteranganProduk) {
		this.keteranganProduk = keteranganProduk;
	}
	
	@Column(name="ID_KATEGORI")
	public Integer getIdKategori() {
		return idKategori;
	}

	public void setIdKategori(Integer idKategori) {
		this.idKategori = idKategori;
	}

	@ManyToOne 
	@JoinColumn(name="ID_KATEGORI", nullable=true, updatable=false, insertable=false)
	public KategoriModel getKategoriModel() {
		return kategoriModel;
	}

	public void setKategoriModel(KategoriModel kategoriModel) {
		this.kategoriModel = kategoriModel;
	}
	
	@Column(name="ID_SUPPLIER")
	public Integer getIdSupplier() {
		return idSupplier;
	}

	public void setIdSupplier(Integer idSupplier) {
		this.idSupplier = idSupplier;
	}

	@ManyToOne
	@JoinColumn(name="ID_SUPPLIER", nullable=true, updatable=false, insertable=false)
	public SupplierModel getSupplierModel() {
		return supplierModel;
	}

	public void setSupplierModel(SupplierModel supplierModel) {
		this.supplierModel = supplierModel;
	}

	@Column(name="X_CREATED_BY_PRODUK")
	public String getxCreatedByProduk() {
		return xCreatedByProduk;
	}

	public void setxCreatedByProduk(String xCreatedByProduk) {
		this.xCreatedByProduk = xCreatedByProduk;
	}

	@ManyToOne
	@JoinColumn(name = "X_CREATED_BY_PRODUK", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedByUserProduk() {
		return xCreatedByUserProduk;
	}

	public void setxCreatedByUserProduk(UserModel xCreatedByUserProduk) {
		this.xCreatedByUserProduk = xCreatedByUserProduk;
	}

	@Column(name = "X_CREATED_DATE_PRODUK")
	public Date getxCreatedDateProduk() {
		return xCreatedDateProduk;
	}

	public void setxCreatedDateProduk(Date xCreatedDateProduk) {
		this.xCreatedDateProduk = xCreatedDateProduk;
	}

	@Column (name = "X_UPDATED_BY_PRODUK")
	public String getxUpdatedByProduk() {
		return xUpdatedByProduk;
	}

	public void setxUpdatedByProduk(String xUpdatedByProduk) {
		this.xUpdatedByProduk = xUpdatedByProduk;
	}

	@ManyToOne
	@JoinColumn(name = "X_UPDATED_BY_PRODUK", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedByUserProduk() {
		return xUpdatedByUserProduk;
	}

	public void setxUpdatedByUserProduk(UserModel xUpdatedByUserProduk) {
		this.xUpdatedByUserProduk = xUpdatedByUserProduk;
	}

	@Column (name = "X_UPDATED_DATE_PRODUK")
	public Date getxUpdatedDateProduk() {
		return xUpdatedDateProduk;
	}

	public void setxUpdatedDateProduk(Date xUpdatedDateProduk) {
		this.xUpdatedDateProduk = xUpdatedDateProduk;
	}

	@Column (name = "X_IS_DELETE_PRODUK")
	public Integer getxIsDeleteProduk() {
		return xIsDeleteProduk;
	}

	public void setxIsDeleteProduk(Integer xIsDeleteProduk) {
		this.xIsDeleteProduk = xIsDeleteProduk;
	}
	
}
