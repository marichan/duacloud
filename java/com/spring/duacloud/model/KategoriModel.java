package com.spring.duacloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_KATEGORI")
public class KategoriModel {
	
	private Integer idKategori; //PK dinamis
	
	private String kodeKategori;
	private String namaKategori;
	private String keteranganKategori;
	
	//set audit trail
	private String xCreatedByKategori;
	private UserModel xCreatedByUserKategori;
		
	private Date xCreatedDateKategori;
		
	private String xUpdatedByKategori;
	private UserModel xUpdatedByUserKategori;
		
	private Date xUpdatedDateKategori;
	private Integer xIsDeleteKategori;
	
	@Id
	@Column(name="ID_KATEGORI")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_KATEGORI")
	@TableGenerator(name="M_KATEGORI", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME", pkColumnValue="M_ID_KATEGORI", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(Integer idKategori) {
		this.idKategori = idKategori;
	}
	
	@Column(name = "KODE_KATEGORI")	// nullable = false karena ada Id
	public String getKodeKategori() {
		return kodeKategori;
	}
	public void setKodeKategori(String kodeKategori) {
		this.kodeKategori = kodeKategori;
	}
	
	@Column(name = "NAMA_KATEGORI", nullable = true)
	public String getNamaKategori() {
		return namaKategori;
	}
	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	@Column(name = "KETERANGAN_KATEGORI", nullable = true)
	public String getKeteranganKategori() {
		return keteranganKategori;
	}
	public void setKeteranganKategori(String keteranganKategori) {
		this.keteranganKategori = keteranganKategori;
	}
	
	@Column(name="X_CREATED_BY_KATEGORI")
	public String getxCreatedByKategori() {
		return xCreatedByKategori;
	}
	public void setxCreatedByKategori(String xCreatedByKategori) {
		this.xCreatedByKategori = xCreatedByKategori;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_CREATED_BY_KATEGORI", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedByUserKategori() {
		return xCreatedByUserKategori;
	}
	public void setxCreatedByUserKategori(UserModel xCreatedByUserKategori) {
		this.xCreatedByUserKategori = xCreatedByUserKategori;
	}
	
	@Column(name = "X_CREATED_DATE_KATEGORI")
	public Date getxCreatedDateKategori() {
		return xCreatedDateKategori;
	}
	public void setxCreatedDateKategori(Date xCreatedDateKategori) {
		this.xCreatedDateKategori = xCreatedDateKategori;
	}
	
	@Column (name = "X_UPDATED_BY_KATEGORI")
	public String getxUpdatedByKategori() {
		return xUpdatedByKategori;
	}
	public void setxUpdatedByKategori(String xUpdatedByKategori) {
		this.xUpdatedByKategori = xUpdatedByKategori;
	}
	
	@ManyToOne
	@JoinColumn(name = "X_UPDATED_BY_KATEGORI", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedByUserKategori() {
		return xUpdatedByUserKategori;
	}
	public void setxUpdatedByUserKategori(UserModel xUpdatedByUserKategori) {
		this.xUpdatedByUserKategori = xUpdatedByUserKategori;
	}
	
	@Column (name = "X_UPDATED_DATE_KATEGORI")
	public Date getxUpdatedDateKategori() {
		return xUpdatedDateKategori;
	}
	public void setxUpdatedDateKategori(Date xUpdatedDateKategori) {
		this.xUpdatedDateKategori = xUpdatedDateKategori;
	}
	
	@Column (name = "X_IS_DELETE_KATEGORI")
	public Integer getxIsDeleteKategori() {
		return xIsDeleteKategori;
	}
	public void setxIsDeleteKategori(Integer xIsDeleteKategori) {
		this.xIsDeleteKategori = xIsDeleteKategori;
	}
	
}
