package com.spring.duacloud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="M_ROLE")
public class RoleModel {
	private String kodeRole;
	private String namaRole;
	
	@Id
	@Column(name="KODE_ROLE")
	public String getKodeRole() {
		return kodeRole;
	}
	public void setKodeRole(String kodeRole) {
		this.kodeRole = kodeRole;
	}
	
	@Column(name="NAMA_ROLE")
	public String getNamaRole() {
		return namaRole;
	}
	public void setNamaRole(String namaRole) {
		this.namaRole = namaRole;
	}
	
	
}
