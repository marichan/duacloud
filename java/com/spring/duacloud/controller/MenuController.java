package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.service.MenuService;

@Controller
public class MenuController extends BaseController {

	@Autowired
	private MenuService menuService;

	public void accessLogin(Model model) {

		// method untuk menampilkan menu by role login dan username

		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen",
				this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan",
				this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());

		// logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		// akhir method

	}

	@RequestMapping(value = "menu")
	public String menuMethod(Model model) {
		String halaman = "menu";

		// akses login itu method dari login controller
		this.accessLogin(model);

		return halaman;
	}

	@RequestMapping(value = "menu/cari")
	public String cariMenuMethod(Model model, HttpServletRequest request) {
		String halaman = "menu/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuModelList = this.menuService.cariMenu(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("menuModelList", menuModelList);
		return halaman;
	}

	@RequestMapping(value = "menu/list")
	public String listMenuMethod(Model model) {
		String halaman = "menu/list";
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("menuModelList", menuModelList);

		return halaman;
	}

	@RequestMapping(value = "menu/tambah")
	public String tambahMenuMethod(Model model) {
		String halaman = "menu/add";
		return halaman;
	}

	@RequestMapping(value = "menu/simpan_delete")
	public String simpanDeleteMenuMethod(HttpServletRequest request) {
		String halaman = "menu";

		String kodeMenu = request.getParameter("kodeMenu");

		MenuModel menuModelDB = new MenuModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			menuModelDB = this.menuService.cariKode(kodeMenu);

			this.menuService.delete(menuModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menu/simpan_edit")
	public String simpanEditMenuMethod(HttpServletRequest request) {
		String halaman = "menu";

		String kodeMenu = request.getParameter("kodeMenu");
		String namaMenu = request.getParameter("namaMenu");
		String controllerMenu = request.getParameter("controllerMenu");

		MenuModel menuModelDB = new MenuModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			menuModelDB = this.menuService.cariKode(kodeMenu);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			menuModelDB.setNamaMenu(namaMenu);
			menuModelDB.setControllerMenu(controllerMenu);

			this.menuService.update(menuModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menu/simpan_add")
	public String simpanAddMenuMethod(HttpServletRequest request) {
		String halaman = "menu";

		String kodeMenu = request.getParameter("kodeMenu");
		String namaMenu = request.getParameter("namaMenu");
		String controllerMenu = request.getParameter("controllerMenu");

		MenuModel menuModel = new MenuModel();

		menuModel.setKodeMenu(kodeMenu);
		menuModel.setNamaMenu(namaMenu);
		menuModel.setControllerMenu(controllerMenu);

		try {
			this.menuService.create(menuModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menu/ubah")
	public String ubahMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/edit";
		String kodeMenu = request.getParameter("kodeMenu");

		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModel", menuModel);

		return halaman;
	}

	@RequestMapping(value = "menu/hapus")
	public String hapusMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/delete";
		String kodeMenu = request.getParameter("kodeMenu");

		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModel", menuModel);

		return halaman;
	}

	@RequestMapping(value = "menu/detail")
	public String detailMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/detail";
		String kodeMenu = request.getParameter("kodeMenu");

		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModel", menuModel);
		return halaman;
	}

	@RequestMapping(value = "hasil_menu")
	public String hasilMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/hasil_menu";

		String kodeMenu = request.getParameter("kodeMenu");
		String namaMenu = request.getParameter("namaMenu");
		String controllerMenu = request.getParameter("controllerMenu");

		model.addAttribute("kodeMenu", kodeMenu);
		model.addAttribute("namaMenu", namaMenu);
		model.addAttribute("controllerMenu", controllerMenu);

		return halaman;
	}

}
