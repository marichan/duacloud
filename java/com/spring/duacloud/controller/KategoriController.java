package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.KategoriModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.service.KategoriService;
import com.spring.duacloud.service.MenuService;

@Controller
public class KategoriController extends BaseController {
	
	@Autowired
	private KategoriService kategoriService; 
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value="kategori")
	public String kategoriMethod(Model model) {
		String halaman = "kategori";
		this.accessLogin(model);
		return halaman;
	}
	
	@RequestMapping(value="kategori/cari")
	public String cariKategoriMethod(Model model, HttpServletRequest request) {
		String halaman = "kategori/list";
		
		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");
				
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		
		try {
			kategoriModelList = this.kategoriService.cariKategori(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		model.addAttribute("kategoriModelList", kategoriModelList);
		return halaman;
	}
	
	@RequestMapping(value="kategori/list")
	public String listKategoriMethod(Model model) {
		String halaman = "kategori/list";
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		
		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		model.addAttribute("kategoriModelList", kategoriModelList);
		
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/tambah")
	public String tambahKategoriMethod(Model model) throws Exception 
	{
		String halaman = "kategori/add";
		
		Integer seqIdKategori = this.kategoriService.sequenceValueKategori();
		String kodeKategoriGenerator = null;
		
		if(seqIdKategori < 10) 
		{
			kodeKategoriGenerator = "KAT000" + seqIdKategori;
		}
		else if(seqIdKategori >= 10 && seqIdKategori < 100) 
		{
			kodeKategoriGenerator = "KAT00" + seqIdKategori;
		}
		else if(seqIdKategori >= 100 && seqIdKategori < 1000) 
		{
			kodeKategoriGenerator = "KAT0" + seqIdKategori;
		}
		else if(seqIdKategori >= 1000) 
		{
			kodeKategoriGenerator = "KAT" + seqIdKategori;
		}
		else {}
		
		model.addAttribute("kodeKategoriGenerator", kodeKategoriGenerator);
		return halaman;
	}
	
	@RequestMapping(value="kategori/simpan_delete")
	public String simpanDeleteKategoriMethod(HttpServletRequest request) {
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		
		KategoriModel kategoriModelDB = new KategoriModel();
		
		
		try {
			/*kita cari dulu data yang lama berdasarkan kode/primarykey-nya*/
			kategoriModelDB = this.kategoriService.cariKode(kodeKategori);
			
			//Set xIsDelete Audit Trail 
			String xUpdatedByKategori = this.getUserModel().getKodeUser();
			kategoriModelDB.setxIsDeleteKategori(1);
			kategoriModelDB.setxUpdatedByKategori(xUpdatedByKategori);
			kategoriModelDB.setxUpdatedDateKategori(new Date());
			
			this.kategoriService.update(kategoriModelDB);
			
			//this.kategoriService.delete(kategoriModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/simpan_edit")
	public String simpanEditKategoriMethod(HttpServletRequest request) {
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		String namaKategori = request.getParameter("namaKategori");
		String keteranganKategori = request.getParameter("keteranganKategori");
		
		KategoriModel kategoriModelDB = new KategoriModel();
		
		
		try {
			/*kita cari dulu data yang lama berdasarkan kode/primarykey-nya*/
			kategoriModelDB = this.kategoriService.cariKode(kodeKategori);
			
			/*kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah saja*/
			kategoriModelDB.setNamaKategori(namaKategori);
			kategoriModelDB.setKeteranganKategori(keteranganKategori);
			
			//set Audit Trail
			String xUpdatedByKategori = this.getUserModel().getKodeUser();
			kategoriModelDB.setxUpdatedByKategori(xUpdatedByKategori);
			kategoriModelDB.setxUpdatedDateKategori(new Date());
			
			this.kategoriService.update(kategoriModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/simpan_add")
	public String simpanAddKategoriMethod(HttpServletRequest request, Model model) throws Exception
	{
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		
		//cek data saat Tambah
		KategoriModel kategoriModelDB = null;
		
		//panggil by Id
		kategoriModelDB = this.kategoriService.cariKode(kodeKategori);
		
		//buat varible untuk cek data null
		String kodeKategoriExist = null;
		
		//untuk Baca
		if (kategoriModelDB != null) {
			kodeKategoriExist = "yes";
		}
		
		//cari Nama
		String namaKategori = request.getParameter("namaKategori");
		List<KategoriModel> kategoriModelListDB = new ArrayList<KategoriModel>();
		
		kategoriModelListDB = this.kategoriService.cariNamaKategori(namaKategori);
		String namaKategoriExist = null;
		
		if(kategoriModelListDB.size() > 0) 
		{
			namaKategoriExist = "yes";
		}
		
		//validasi cek null
		if (kodeKategoriExist == "yes") {
			//kalau tidak null 
			model.addAttribute("kodeKategori", kodeKategori);
			model.addAttribute("kodeKategoriExist", kodeKategoriExist);
		} 
		
		else if(namaKategoriExist == "yes") 
		{
			model.addAttribute("namaKategori", namaKategori);
			model.addAttribute("namaKategoriExist", namaKategoriExist);
		}
		
		else {
			//kalau null
			
			//String namaKategori = request.getParameter("namaKategori");
			String keteranganKategori = request.getParameter("keteranganKategori");
			
			KategoriModel kategoriModel = new KategoriModel();
			
			kategoriModel.setKodeKategori(kodeKategori);
			kategoriModel.setNamaKategori(namaKategori);
			kategoriModel.setKeteranganKategori(keteranganKategori);
			
			//set Audit Trail
			String xCreatedByKategori = this.getUserModel().getKodeUser();
			kategoriModel.setxCreatedByKategori(xCreatedByKategori);
			kategoriModel.setxCreatedDateKategori(new Date());
			kategoriModel.setxIsDeleteKategori(0);
			 
			this.kategoriService.create(kategoriModel);
		} 
		return halaman;
	}
	
	@RequestMapping(value="kategori/ubah")
	public String ubahKategoriMethod(HttpServletRequest request, Model model) {
		String halaman = "kategori/edit";
		String kodeKategori = request.getParameter("kodeKategori");
		
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("kategoriModel", kategoriModel);
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/hapus")
	public String hapusKategoriMethod(HttpServletRequest request, Model model) {
		String halaman = "kategori/delete";
		String kodeKategori = request.getParameter("kodeKategori");
		
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("kategoriModel", kategoriModel);
		return halaman;
	}
	
	@RequestMapping(value="kategori/detail")
	public String detailKategoriMethod(HttpServletRequest request, Model model) {
		String halaman = "kategori/detail";
		String kodeKategori = request.getParameter("kodeKategori");
		
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("kategoriModel", kategoriModel);
		return halaman;
	}
	
	@RequestMapping(value="kategori/detail/subdetail")
	public String subDetailKategoriMethod() {
		String halaman = "kategori/subdetail";
		return halaman;
	}
	
	@RequestMapping(value="kategori/detail/subedit")
	public String subEditKategoriMethod() {
		String halaman = "kategori/subedit";
		return halaman;
	}
	
	@RequestMapping(value="kategori/detail/subdelete")
	public String subDeleteKategoriMethod() {
		String halaman = "kategori/subdelete";
		return halaman;
	}
	
	@RequestMapping(value="hasil_kategori")
	public String hasilKategoriMethod(HttpServletRequest request, Model model) {
		String halaman = "kategori/hasil_kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		String namaKategori = request.getParameter("namaKategori");
		String keteranganKategori = request.getParameter("keteranganKategori");
		
		model.addAttribute("kodeKategori", kodeKategori);
		model.addAttribute("namaKategori", namaKategori);
		model.addAttribute("keteranganKategori", keteranganKategori);
		
		return halaman;
	}
}
