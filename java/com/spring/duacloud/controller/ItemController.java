package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.ItemModel;
import com.spring.duacloud.model.KategoriModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.SupplierModel;
import com.spring.duacloud.service.ItemService;
import com.spring.duacloud.service.KategoriService;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.SupplierService;

@Controller
public class ItemController extends BaseController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private KategoriService kategoriService;

	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private MenuService menuService;

	public void accessLogin(Model model) {

		// method untuk menampilkan menu by role login dan username

		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen",
				this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan",
				this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());

		// logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		// akhir method

	}

	@RequestMapping(value = "item")
	public String itemMethod(Model model) {
		String halaman = "item";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "item/cari")
	public String cariItemMethod(Model model, HttpServletRequest request) {
		String halaman = "item/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<ItemModel> itemModelList = new ArrayList<ItemModel>();

		try {
			itemModelList = this.itemService.cariItem(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("itemModelList", itemModelList);
		return halaman;
	}

	@RequestMapping(value = "item/list")
	public String listItemMethod(Model model) {
		String halaman = "item/list";
		List<ItemModel> itemModelList = new ArrayList<ItemModel>();

		try {
			itemModelList = this.itemService.listIsNotDelete();
			//itemModelList = this.itemService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("itemModelList", itemModelList);

		return halaman;
	}

	@RequestMapping(value = "item/tambah")
	public String tambahItemMethod(Model model) throws Exception
	{
		String halaman = "item/add";

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierService.listIsNotDelete();
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierService.list();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		//generator Id 
		Integer seqIdItem = this.itemService.sequenceValueItem();
		String kodeItemGenerator = null;
		
		if (seqIdItem < 10) {
			kodeItemGenerator = "ITM000" + seqIdItem;
		} 
		else if(seqIdItem >= 10 && seqIdItem < 100) {
			kodeItemGenerator = "ITM00" + seqIdItem;
		}
		else if(seqIdItem >= 100 && seqIdItem < 1000) {
			kodeItemGenerator = "ITM0" + seqIdItem;
		}
		else if(seqIdItem >= 1000) {
			kodeItemGenerator = "ITM" + seqIdItem;
		}
		else {}
		
		model.addAttribute("kodeItemGenerator", kodeItemGenerator);
		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMaster

		return halaman;
	}

	@RequestMapping(value = "item/simpan_delete")
	public String simpanDeleteItemMethod(HttpServletRequest request) {
		String halaman = "item";

		String kodeItem = request.getParameter("kodeItem");

		ItemModel itemModelDB = new ItemModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			itemModelDB = this.itemService.cariKode(kodeItem);

			String xUpdatedByItem = this.getUserModel().getKodeUser();
			itemModelDB.setxIsDeleteItem(1);
			itemModelDB.setxUpdatedByItem(xUpdatedByItem);
			itemModelDB.setxUpdatedDateItem(new Date());
			
			this.itemService.update(itemModelDB);
			
			//this.itemService.delete(itemModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "item/simpan_edit")
	public String simpanEditItemMethod(HttpServletRequest request) {
		String halaman = "item";

		String kodeItem = request.getParameter("kodeItem");
		String namaItem = request.getParameter("namaItem");

		// save join table
		Integer idKategori = Integer.valueOf(request.getParameter("idKategori"));
		Integer idSupplier = Integer.valueOf(request.getParameter("idSupplier"));
		// save join table

		Integer hargaJual = Integer.valueOf(request.getParameter("hargaJual"));
		String keteranganItem = request.getParameter("keteranganItem");

		ItemModel itemModelDB = new ItemModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			itemModelDB = this.itemService.cariKode(kodeItem);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			itemModelDB.setNamaItem(namaItem);

			// save join table
			itemModelDB.setIdKategori(idKategori);
			itemModelDB.setIdSupplier(idSupplier);
			// save join table

			itemModelDB.setHargaJual(hargaJual);
			itemModelDB.setKeteranganItem(keteranganItem);

			//set audit trail
			String xUpdatedByItem = this.getUserModel().getKodeUser();
			itemModelDB.setxUpdatedByItem(xUpdatedByItem);
			itemModelDB.setxUpdatedDateItem(new Date());
			
			this.itemService.update(itemModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "item/simpan_add")
	public String simpanAddItemMethod(HttpServletRequest request, Model model) throws Exception 
	{
		String halaman = "item";

		String kodeItem = request.getParameter("kodeItem");
		
		//cek data saat tambah
		ItemModel itemModelDB = null;
		
		//panggil by Id
		itemModelDB = this.itemService.cariKode(kodeItem);
		
		//buat Variable untuk cek data null
		String kodeItemExist = null;
		
		if(itemModelDB != null)
		{
			kodeItemExist = "yes";
		}
		
		//cari nama
		String namaItem = request.getParameter("namaItem");
		List<ItemModel> itemModelListDB = new ArrayList<ItemModel>();
		
		itemModelListDB = this.itemService.cariNamaItem(namaItem);
		String namaItemExist = null;
		
		if(itemModelListDB.size() > 0) 
		{
			namaItemExist = "yes";
		}
		
		//validasi cek null
		if (kodeItemExist == "yes") {
			//kalau tidak null 
			model.addAttribute("kodeItem", kodeItem);
			model.addAttribute("kodeItemExist", kodeItemExist);
		}
		else if(namaItemExist == "yes") 
		{
			model.addAttribute("namaItem", namaItem);
			model.addAttribute("namaItemExist", namaItemExist);
		}
		else {
			//kalau null
			
			//String namaItem = request.getParameter("namaItem");

			// save join table
			Integer idKategori = Integer.valueOf(request.getParameter("idKategori"));
			Integer idSupplier = Integer.valueOf(request.getParameter("idSupplier"));
			//String kodeKategori = request.getParameter("kodeKategori");
			//String kodeSupplier = request.getParameter("kodeSupplier");
			// save join table

			Integer hargaJual = Integer.valueOf(request.getParameter("hargaJual"));
			String keteranganItem = request.getParameter("keteranganItem");

			ItemModel itemModel = new ItemModel();

			itemModel.setKodeItem(kodeItem);
			itemModel.setNamaItem(namaItem);

			// save join table
			itemModel.setIdKategori(idKategori);
			itemModel.setIdSupplier(idSupplier);
			// save join table

			itemModel.setHargaJual(hargaJual);
			itemModel.setKeteranganItem(keteranganItem);
			
			//set audit trail
			String xCreatedByItem = this.getUserModel().getKodeUser();
			itemModel.setxCreatedByItem(xCreatedByItem);
			itemModel.setxCreatedDateItem(new Date());
			itemModel.setxIsDeleteItem(0);
 
			this.itemService.create(itemModel); 
		}
		 
		return halaman;
	}

	@RequestMapping(value = "item/ubah")
	public String ubahItemMethod(HttpServletRequest request, Model model) {
		String halaman = "item/edit";
		String kodeItem = request.getParameter("kodeItem");

		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("itemModel", itemModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "item/hapus")
	public String hapusItemMethod(HttpServletRequest request, Model model) {
		String halaman = "item/delete";
		String kodeItem = request.getParameter("kodeItem");

		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("itemModel", itemModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "item/detail")
	public String detailItemMethod(HttpServletRequest request, Model model) {
		String halaman = "item/detail";
		String kodeItem = request.getParameter("kodeItem");

		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("itemModel", itemModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "hasil_item")
	public String hasilItemMethod(HttpServletRequest request, Model model) {
		String halaman = "item/hasil_item";

		String kodeItem = request.getParameter("kodeItem");
		String namaItem = request.getParameter("namaItem");
		String kategoriItem = request.getParameter("kategoriItem");
		String supplierItem = request.getParameter("supplierItem");
		Integer hargaJual = Integer.valueOf(request.getParameter("hargaJual"));
		String keteranganItem = request.getParameter("keteranganItem");

		model.addAttribute("kodeItem", kodeItem);
		model.addAttribute("namaItem", namaItem);
		model.addAttribute("kategoriItem", kategoriItem);
		model.addAttribute("supplierItem", supplierItem);
		model.addAttribute("hargaJual", hargaJual);
		model.addAttribute("keteranganItem", keteranganItem);

		return halaman;
	}

}
