package com.spring.duacloud.controller;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.SupplierModel;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.SupplierService;

@Controller
public class SupplierController extends BaseController{
	
	@Autowired
	private SupplierService supplierService; 
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value="supplier/cari")
	public String cariSupplierMethod(Model model, HttpServletRequest request) {
		String halaman = "supplier/list";
		
		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");
				
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		
		try {
			supplierModelList = this.supplierService.cariSupplier(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		model.addAttribute("supplierModelList", supplierModelList);
		return halaman;
	}
	
	@RequestMapping(value="supplier/list")
	public String listSupplierMethod(Model model) {
		String halaman = "supplier/list";
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		
		try {
			//supplierModelList = this.supplierService.list();
			supplierModelList = this.supplierService.listIsNotDelete();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		model.addAttribute("supplierModelList", supplierModelList);
		
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/simpan_delete")
	public String simpanDeleteSupplierMethod(HttpServletRequest request) {
		String halaman = "supplier";
		
		String kodeSupplier = request.getParameter("kodeSupplier");
		
		SupplierModel supplierModelDB = new SupplierModel();
		
		
		try {
			/*kita cari dulu data yang lama berdasarkan kode/primarykey-nya*/
			supplierModelDB = this.supplierService.cariKode(kodeSupplier);
			
			//SET ISDELETE AUDIT TRAIL
			String xUpdatedBySupplier = this.getUserModel().getKodeUser();
			supplierModelDB.setxIsDeleteSupplier(1);
			supplierModelDB.setxUpdatedBySupplier(xUpdatedBySupplier);
			supplierModelDB.setxUpdatedDateSupplier(new Date());
			
			this.supplierService.update(supplierModelDB);//karena tidak hapus secara real
			
			//this.supplierService.delete(supplierModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/simpan_edit")
	public String simpanEditSupplierMethod(HttpServletRequest request) {
		String halaman = "supplier";
		
		String kodeSupplier = request.getParameter("kodeSupplier");
		String namaSupplier = request.getParameter("namaSupplier");
		Integer tipeSupplier = Integer.valueOf(request.getParameter("tipeSupplier"));
		String picSupplier = request.getParameter("picSupplier");
		Integer telpSupplier = Integer.valueOf(request.getParameter("telpSupplier"));
		String emailSupplier = request.getParameter("emailSupplier");
		String alamatSupplier = request.getParameter("alamatSupplier");
		
		
		SupplierModel supplierModelDB = new SupplierModel();
		
		
		try {
			/*kita cari dulu data yang lama berdasarkan kode/primarykey-nya*/
			supplierModelDB = this.supplierService.cariKode(kodeSupplier);
			
			/*kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah saja*/
			supplierModelDB.setNamaSupplier(namaSupplier);
			supplierModelDB.setTipeSupplier(tipeSupplier);
			supplierModelDB.setPicSupplier(picSupplier);
			supplierModelDB.setTelpSupplier(telpSupplier);
			supplierModelDB.setEmailSupplier(emailSupplier);
			supplierModelDB.setAlamatSupplier(alamatSupplier);
			
			//Set audit trail
			String xUpdatedBySupplier= this.getUserModel().getKodeUser();
			supplierModelDB.setxUpdatedBySupplier(xUpdatedBySupplier);
			supplierModelDB.setxUpdatedDateSupplier(new Date());
			
			this.supplierService.update(supplierModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/simpan_add")
	public String simpanAddSupplierMethod(HttpServletRequest request, Model model) throws Exception
	{
		String halaman = "supplier";
		
		String kodeSupplier = request.getParameter("kodeSupplier");
		
		//untuk CEK saat tambah apakah data kosong
		SupplierModel supplierModelDB = null;
		
		//panggil sesuai ID
		supplierModelDB = this.supplierService.cariKode(kodeSupplier);
		
		//buat varibel
		String kodeSupplierExist = null;
		
		//untuk Baca
		if (supplierModelDB != null) { //karena di DAO cariKode = null
			kodeSupplierExist = "yes";
		}
		
		//cari nama
		String namaSupplier = request.getParameter("namaSupplier");
		List<SupplierModel> supplierModelListDB = new ArrayList<SupplierModel>();
		supplierModelListDB = this.supplierService.cariNamaSupplier(namaSupplier);
		
		String namaSupplierExist = null;
		
		if(supplierModelListDB.size() > 0) 
		{
			namaSupplierExist = "yes";
		}
		
		if (kodeSupplierExist == "yes") { //null berarti sudah ada data yg menggunakan kodeSupplier tsb
			//kalau supplierModelDB tidak nulll 
			model.addAttribute("kodeSupplier", kodeSupplier);
			model.addAttribute("kodeSupplierExist", kodeSupplierExist);
		} 
		
		else if(namaSupplierExist == "yes") 
		{
			//null berarti sudah ada data yg menggunakan namaSupplier tsb
			model.addAttribute("namaSupplier", namaSupplier);
			model.addAttribute("namaSupplierExist", namaSupplierExist);
		}
		
		else {
			//kalau = null -> tidak ada data yg menggunakan kodeSupplier tsb
			
			//String namaSupplier = request.getParameter("namaSupplier");
			Integer tipeSupplier = Integer.valueOf(request.getParameter("tipeSupplier"));
			String picSupplier = request.getParameter("picSupplier");
			Integer telpSupplier = Integer.valueOf(request.getParameter("telpSupplier"));
			String emailSupplier = request.getParameter("emailSupplier");
			String alamatSupplier = request.getParameter("alamatSupplier");
			
			SupplierModel supplierModel = new SupplierModel();
			
			supplierModel.setKodeSupplier(kodeSupplier);
			supplierModel.setNamaSupplier(namaSupplier);
			supplierModel.setTipeSupplier(tipeSupplier);
			supplierModel.setPicSupplier(picSupplier);
			supplierModel.setTelpSupplier(telpSupplier);
			supplierModel.setEmailSupplier(emailSupplier);
			supplierModel.setAlamatSupplier(alamatSupplier);
			 
			//Set Audit Trail
			String xCreatedBySupplier = this.getUserModel().getKodeUser(); //primary key yg di set
			supplierModel.setxCreatedBySupplier(xCreatedBySupplier);
			supplierModel.setxCreatedDateSupplier(new Date());
			supplierModel.setxIsDeleteSupplier(0); //hanya di delete di view, di DB masih ada untuk history
	
			this.supplierService.create(supplierModel);
			
		}  
		return halaman;
	}
	
	@RequestMapping(value="supplier")
	public String supplierMethod(Model model) {
		String halaman = "supplier";
		this.accessLogin(model);
		return halaman;
	}
	
	@RequestMapping(value="supplier/tambah")
	public String tambahSupplierMethod(Model model) throws Exception
	{
		String halaman = "supplier/add";
		Integer seqIdSupplier = this.supplierService.sequenceValueSupplier();
		String kodeSupplierGenerator = null;
		
		if (seqIdSupplier < 10) {
			kodeSupplierGenerator = "SUP000"+seqIdSupplier;
		} 
		else if(seqIdSupplier >= 10 && seqIdSupplier < 100) {
			kodeSupplierGenerator = "SUP00"+seqIdSupplier;
		}
		else if(seqIdSupplier >= 100 && seqIdSupplier < 1000) {
			kodeSupplierGenerator = "SUP0"+seqIdSupplier;
		}
		else if(seqIdSupplier >= 1000) {
			kodeSupplierGenerator = "SUP"+seqIdSupplier;
		}
		else {

		}
		model.addAttribute("kodeSupplierGenerator", kodeSupplierGenerator);
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/ubah")
	public String ubahSupplierMethod(HttpServletRequest request, Model model) {
		String halaman = "supplier/edit";
		String kodeSupplier = request.getParameter("kodeSupplier");
		
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("supplierModel", supplierModel);
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/hapus")
	public String hapusSupplierMethod(HttpServletRequest request, Model model) {
		String halaman = "supplier/delete";
		String kodeSupplier = request.getParameter("kodeSupplier");
		
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("supplierModel", supplierModel);
		return halaman;
	}
	
	@RequestMapping(value="supplier/detail")
	public String detailSupplierMethod(HttpServletRequest request, Model model) {
		String halaman = "supplier/detail";
		String kodeSupplier = request.getParameter("kodeSupplier");
		
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("supplierModel", supplierModel);
		return halaman;
	}
	
	

	@RequestMapping(value="hasil_supplier")
	public String hasilSupplierMethod(HttpServletRequest request, Model model) {
		String halaman = "supplier/hasil_supplier";
		
		String kodeSupplier = request.getParameter("kodeSupplier");
		String namaSupplier = request.getParameter("namaSupplier");
		Integer tipe = Integer.valueOf(request.getParameter("tipe"));
		String picSupplier = request.getParameter("picSupplier");
		Integer telpSupplier = Integer.valueOf(request.getParameter("telpSupplier"));
		String email = request.getParameter("email");
		String alamat = request.getParameter("alamat");
	
		model.addAttribute("kodeSupplier", kodeSupplier);
		model.addAttribute("namaSupplier", namaSupplier);
		model.addAttribute("tipe", tipe);
		model.addAttribute("picSupplier", picSupplier);
		model.addAttribute("telpSupplier", telpSupplier);
		model.addAttribute("email", email);
		model.addAttribute("alamat", alamat);
		
		return halaman;
	}
	
}
