package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.RoleModel;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.RoleService;

@Controller
public class RoleController extends BaseController{
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "role")
	public String roleMethod(Model model) {
		String halaman = "role";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "role/cari")
	public String cariRoleMethod(Model model, HttpServletRequest request) {
		String halaman = "role/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.cariRole(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("roleModelList", roleModelList);
		return halaman;
	}

	@RequestMapping(value = "role/list")
	public String listRoleMethod(Model model) {
		String halaman = "role/list";
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("roleModelList", roleModelList);

		return halaman;
	}

	@RequestMapping(value = "role/tambah")
	public String tambahRoleMethod(Model model) {
		String halaman = "role/add";
		return halaman;
	}

	@RequestMapping(value = "role/simpan_delete")
	public String simpanDeleteRoleMethod(HttpServletRequest request) {
		String halaman = "role";

		String kodeRole = request.getParameter("kodeRole");

		RoleModel roleModelDB = new RoleModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			roleModelDB = this.roleService.cariKode(kodeRole);

			this.roleService.delete(roleModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "role/simpan_edit")
	public String simpanEditRoleMethod(HttpServletRequest request) {
		String halaman = "role";

		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");

		RoleModel roleModelDB = new RoleModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			roleModelDB = this.roleService.cariKode(kodeRole);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			roleModelDB.setNamaRole(namaRole);

			this.roleService.update(roleModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "role/simpan_add")
	public String simpanAddRoleMethod(HttpServletRequest request) {
		String halaman = "role";

		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");

		RoleModel roleModel = new RoleModel();

		roleModel.setKodeRole(kodeRole);
		roleModel.setNamaRole(namaRole);

		try {
			this.roleService.create(roleModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "role/ubah")
	public String ubahRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/edit";
		String kodeRole = request.getParameter("kodeRole");

		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("roleModel", roleModel);

		return halaman;
	}

	@RequestMapping(value = "role/hapus")
	public String hapusRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/delete";
		String kodeRole = request.getParameter("kodeRole");

		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("roleModel", roleModel);

		return halaman;
	}

	@RequestMapping(value = "role/detail")
	public String detailRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/detail";
		String kodeRole = request.getParameter("kodeRole");

		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("roleModel", roleModel);
		return halaman;
	}

	@RequestMapping(value = "hasil_role")
	public String hasilRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/hasil_role";

		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");

		model.addAttribute("kodeRole", kodeRole);
		model.addAttribute("namaRole", namaRole);

		return halaman;
	}
	
}
