package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.DepartemenModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.service.DepartemenService;
import com.spring.duacloud.service.MenuService;


@Controller
public class DepartemenController extends BaseController{
	
	@Autowired
	private DepartemenService departemenService;
	
	@Autowired
	private MenuService menuService;
	
public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "departemen")
	public String departemenMethod(Model model) {
		String halaman = "departemen";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "departemen/cari")
	public String cariDepartemenMethod(Model model, HttpServletRequest request) {
		String halaman = "departemen/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.cariDepartemen(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("departemenModelList", departemenModelList);
		return halaman;
	}

	@RequestMapping(value = "departemen/list")
	public String listDepartemenMethod(Model model) {
		String halaman = "departemen/list";
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("departemenModelList", departemenModelList);

		return halaman;
	}

	@RequestMapping(value = "departemen/tambah")
	public String tambahDepartemenMethod(Model model) {
		String halaman = "departemen/add";
		return halaman;
	}

	@RequestMapping(value = "departemen/simpan_delete")
	public String simpanDeleteDepartemenMethod(HttpServletRequest request) {
		String halaman = "departemen";

		String kodeDepartemen = request.getParameter("kodeDepartemen");

		DepartemenModel departemenModelDB = new DepartemenModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			departemenModelDB = this.departemenService.cariKode(kodeDepartemen);

			this.departemenService.delete(departemenModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "departemen/simpan_edit")
	public String simpanEditDepartemenMethod(HttpServletRequest request) {
		String halaman = "departemen";

		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String namaDepartemen = request.getParameter("namaDepartemen");
		String telpDepartemen = request.getParameter("telpDepartemen");

		DepartemenModel departemenModelDB = new DepartemenModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			departemenModelDB = this.departemenService.cariKode(kodeDepartemen);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			departemenModelDB.setNamaDepartemen(namaDepartemen);
			departemenModelDB.setTelpDepartemen(telpDepartemen);

			this.departemenService.update(departemenModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "departemen/simpan_add")
	public String simpanAddDepartemenMethod(HttpServletRequest request) {
		String halaman = "departemen";

		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String namaDepartemen = request.getParameter("namaDepartemen");
		String telpDepartemen = request.getParameter("telpDepartemen");

		DepartemenModel departemenModel = new DepartemenModel();

		departemenModel.setKodeDepartemen(kodeDepartemen);
		departemenModel.setNamaDepartemen(namaDepartemen);
		departemenModel.setTelpDepartemen(telpDepartemen);

		try {
			this.departemenService.create(departemenModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "departemen/ubah")
	public String ubahDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/edit";
		String kodeDepartemen = request.getParameter("kodeDepartemen");

		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("departemenModel", departemenModel);

		return halaman;
	}

	@RequestMapping(value = "departemen/hapus")
	public String hapusDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/delete";
		String kodeDepartemen = request.getParameter("kodeDepartemen");

		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("departemenModel", departemenModel);

		return halaman;
	}

	@RequestMapping(value = "departemen/detail")
	public String detailDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/detail";
		String kodeDepartemen = request.getParameter("kodeDepartemen");

		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("departemenModel", departemenModel);
		return halaman;
	}

	@RequestMapping(value = "hasil_departemen")
	public String hasilDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/hasil_departemen";

		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String namaDepartemen = request.getParameter("namaDepartemen");
		String telpDepartemen = request.getParameter("telpDepartemen");

		model.addAttribute("kodeDepartemen", kodeDepartemen);
		model.addAttribute("namaDepartemen", namaDepartemen);
		model.addAttribute("telpDepartemen", telpDepartemen);

		return halaman;
	}
	
}
