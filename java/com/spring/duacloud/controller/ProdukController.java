package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.KategoriModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.ProdukModel;
import com.spring.duacloud.model.SupplierModel;
import com.spring.duacloud.service.KategoriService;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.ProdukService;
import com.spring.duacloud.service.SupplierService;

@Controller
public class ProdukController extends BaseController{
	
	@Autowired
	private ProdukService produkService;
	
	@Autowired
	private KategoriService kategoriService;

	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value="produk")
	public String produkMethod(Model model) {
		String halaman = "produk";
		this.accessLogin(model);
		return halaman;
	}
	
	@RequestMapping(value = "produk/cari")
	public String cariProdukMethod(Model model, HttpServletRequest request) {
		String halaman = "produk/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();

		try {
			produkModelList = this.produkService.cariProduk(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("produkModelList", produkModelList);
		return halaman;
	}

	@RequestMapping(value = "produk/list")
	public String listProdukMethod(Model model) {
		String halaman = "produk/list";
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();

		try {
			produkModelList = this.produkService.listIsNotDelete();
			//produkModelList = this.produkService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("produkModelList", produkModelList);

		return halaman;
	}
	
	@RequestMapping(value = "produk/tambah")
	public String tambahProdukMethod(Model model) throws Exception
	{
		String halaman = "produk/add";

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierService.listIsNotDelete();
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierService.list();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		//Generator ID
		Integer seqIdProduk = this.produkService.sequenceValueProduk();
		String kodeProdukGenerator = null;
		
		if(seqIdProduk < 10) 
		{
			kodeProdukGenerator = "PRD000" + seqIdProduk;
		}
		else if(seqIdProduk >= 10 && seqIdProduk < 100) 
		{
			kodeProdukGenerator = "PRD00" + seqIdProduk;
		}
		else if(seqIdProduk >= 100 && seqIdProduk < 1000) 
		{
			kodeProdukGenerator = "PRD0" + seqIdProduk;
		}
		else if(seqIdProduk >= 1000) 
		{
			kodeProdukGenerator = "PRD" + seqIdProduk;
		}
		else {}
		
		model.addAttribute("kodeProdukGenerator", kodeProdukGenerator);
		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "produk/simpan_delete")
	public String simpanDeleteProdukMethod(HttpServletRequest request) {
		String halaman = "produk";

		String kodeProduk = request.getParameter("kodeProduk");

		ProdukModel produkModelDB = new ProdukModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			produkModelDB = this.produkService.cariKode(kodeProduk);

			//Set xIsDelete Audit Trail  
			String xUpdatedByProduk = this.getUserModel().getKodeUser();
			produkModelDB.setxIsDeleteProduk(1);
			produkModelDB.setxUpdatedByProduk(xUpdatedByProduk);
			produkModelDB.setxUpdatedDateProduk(new Date());
			
			this.produkService.update(produkModelDB);
			
			//this.produkService.delete(produkModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "produk/simpan_edit")
	public String simpanEditProdukMethod(HttpServletRequest request) {
		String halaman = "produk";

		String kodeProduk = request.getParameter("kodeProduk");
		String namaProduk = request.getParameter("namaProduk");

		// save join table
		Integer idKategori = Integer.valueOf(request.getParameter("idKategori"));
		Integer idSupplier = Integer.valueOf(request.getParameter("idSupplier"));
		// save join table
		
		Integer hargaProduk = Integer.valueOf(request.getParameter("hargaProduk"));
		Integer biayaProduk = Integer.valueOf(request.getParameter("biayaProduk"));
		Integer hargaTotal = Integer.valueOf(request.getParameter("hargaTotal"));
		String keteranganProduk = request.getParameter("keteranganProduk");

		ProdukModel produkModelDB = new ProdukModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			produkModelDB = this.produkService.cariKode(kodeProduk);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			produkModelDB.setNamaProduk(namaProduk);

			// save join table
			produkModelDB.setIdKategori(idKategori);
			produkModelDB.setIdSupplier(idSupplier); 
			// save join table

			produkModelDB.setHargaProduk(hargaProduk);
			produkModelDB.setBiayaProduk(biayaProduk);
			produkModelDB.setHargaTotal(hargaTotal);
			produkModelDB.setKeteranganProduk(keteranganProduk);

			//set audit trail
			String xUpdatedByProduk = this.getUserModel().getKodeUser();
			produkModelDB.setxUpdatedByProduk(xUpdatedByProduk);
			produkModelDB.setxUpdatedDateProduk(new Date());
			
			this.produkService.update(produkModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "produk/simpan_add")
	public String simpanAddProdukMethod(HttpServletRequest request, Model model) throws Exception
	{
		String halaman = "produk";

		String kodeProduk = request.getParameter("kodeProduk");
		
		//cek data saat tambah
		ProdukModel produkModelDB = null;
		
		//panggil by Id
		produkModelDB = this.produkService.cariKode(kodeProduk);
		
		//buat variable untuk cek data null
		String kodeProdukExist = null;
		
		//untuk baca
		if(produkModelDB != null) 
		{
			kodeProdukExist = "yes";
		}
		
		//cari Nama
		String namaProduk = request.getParameter("namaProduk");
		List<ProdukModel> produkModelListDB = new ArrayList<ProdukModel>();
		
		produkModelListDB = this.produkService.cariNamaProduk(namaProduk);
		String namaProdukExist = null;
		
		if(produkModelListDB.size() > 0) 
		{
			namaProdukExist = "yes";
		}
		
		//validasi cek null
		if (kodeProdukExist == "yes") {
			//kalau tidak null 
			model.addAttribute("kodeProduk", kodeProduk);
			model.addAttribute("kodeProdukExist", kodeProdukExist);
		}
		else if(namaProdukExist == "yes") 
		{
			model.addAttribute("namaProduk", namaProduk);
			model.addAttribute("namaProdukExist", namaProdukExist);
		}
		else {
			//kalau null
			
			//String namaProduk = request.getParameter("namaProduk");

			// save join table
			Integer idKategori = Integer.valueOf(request.getParameter("idKategori"));
			Integer idSupplier = Integer.valueOf(request.getParameter("idSupplier"));
			// save join table

			Integer hargaProduk = Integer.valueOf(request.getParameter("hargaProduk"));
			Integer biayaProduk = Integer.valueOf(request.getParameter("biayaProduk"));
			Integer hargaTotal = Integer.valueOf(request.getParameter("hargaTotal"));
			String keteranganProduk = request.getParameter("keteranganProduk");

			ProdukModel produkModel = new ProdukModel();

			produkModel.setKodeProduk(kodeProduk);
			produkModel.setNamaProduk(namaProduk);

			// save join table
			produkModel.setIdKategori(idKategori);
			produkModel.setIdSupplier(idSupplier); 
			// save join table

			produkModel.setHargaProduk(hargaProduk);
			produkModel.setBiayaProduk(biayaProduk);
			produkModel.setHargaTotal(hargaTotal);
			produkModel.setKeteranganProduk(keteranganProduk);
			
			//set audit trail
			String xCreatedByProduk = this.getUserModel().getKodeUser();
			produkModel.setxCreatedByProduk(xCreatedByProduk);
			produkModel.setxCreatedDateProduk(new Date());
			produkModel.setxIsDeleteProduk(0);
 
			this.produkService.create(produkModel);
		} 
		return halaman;
	}
	
	@RequestMapping(value = "produk/ubah")
	public String ubahProdukMethod(HttpServletRequest request, Model model) {
		String halaman = "produk/edit";
		String kodeProduk = request.getParameter("kodeProduk");

		ProdukModel produkModel = new ProdukModel();
		try {
			produkModel = this.produkService.cariKode(kodeProduk);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("produkModel", produkModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "produk/hapus")
	public String hapusItemMethod(HttpServletRequest request, Model model) {
		String halaman = "produk/delete";
		String kodeProduk = request.getParameter("kodeProduk");

		ProdukModel produkModel = new ProdukModel();
		try {
			produkModel = this.produkService.cariKode(kodeProduk);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("produkModel", produkModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.listIsNotDelete();
			//kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.listIsNotDelete();
			//supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}

	@RequestMapping(value = "produk/detail")
	public String detailProdukMethod(HttpServletRequest request, Model model) {
		String halaman = "produk/detail";
		String kodeProduk = request.getParameter("kodeProduk");

		ProdukModel produkModel = new ProdukModel();
		try {
			produkModel = this.produkService.cariKode(kodeProduk);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("produkModel", produkModel);

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		// skrip untuk select combobox dinamis based on data kategoriMsster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("supplierModelList", supplierModelList);
		// skrip untuk select combobox dinamis based on data kategoriMsster

		return halaman;
	}
	
	@RequestMapping(value="hasil_produk")
	public String hasilProdukMethod() {
		String halaman = "produk/hasil_produk";
		return halaman;
	}
	
	
}
