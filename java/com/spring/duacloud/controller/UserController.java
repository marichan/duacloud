package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.KaryawanModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.RoleModel;
import com.spring.duacloud.model.UserModel;
import com.spring.duacloud.service.KaryawanService;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.RoleService;
import com.spring.duacloud.service.UserService;

@Controller
public class UserController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private KaryawanService karyawanService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "user")
	public String userMethod(Model model) {
		String halaman = "user";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "user/cari")
	public String cariUserMethod(Model model, HttpServletRequest request) {
		String halaman = "user/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<UserModel> userModelList = new ArrayList<UserModel>();

		try {
			userModelList = this.userService.cariUser(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("userModelList", userModelList);
		return halaman;
	}

	@RequestMapping(value = "user/list")
	public String listUserMethod(Model model) {
		String halaman = "user/list";
		List<UserModel> userModelList = new ArrayList<UserModel>();

		try {
			userModelList = this.userService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("userModelList", userModelList);

		return halaman;
	}

	@RequestMapping(value = "user/tambah")
	public String tambahUserMethod(Model model) {
		String halaman = "user/add";

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanService.list();
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("karyawanModelList", karyawanModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "user/simpan_delete")
	public String simpanDeleteUserMethod(HttpServletRequest request) {
		String halaman = "user";

		String kodeUser = request.getParameter("kodeUser");

		UserModel userModelDB = new UserModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			userModelDB = this.userService.cariKode(kodeUser);

			this.userService.delete(userModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "user/simpan_edit")
	public String simpanEditUserMethod(HttpServletRequest request) {
		String halaman = "user";

		String kodeUser = request.getParameter("kodeUser");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		// save join table
		String kodeRole = request.getParameter("kodeRole");
		String kodeKaryawan = request.getParameter("kodeKaryawan");
		// save join table

		UserModel userModelDB = new UserModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			userModelDB = this.userService.cariKode(kodeUser);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			userModelDB.setUsername(username);
			userModelDB.setPassword(password);

			// save join table
			userModelDB.setKodeRole(kodeRole);
			userModelDB.setKodeKaryawan(kodeKaryawan);
			// save join table
			
			this.userService.update(userModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "user/simpan_add")
	public String simpanAddUserMethod(HttpServletRequest request) {
		String halaman = "user";

		String kodeUser = request.getParameter("kodeUser");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		// save join table
		String kodeRole = request.getParameter("kodeRole");
		String kodeKaryawan = request.getParameter("kodeKaryawan");
		
		// save join table

		UserModel userModel = new UserModel();

		userModel.setKodeUser(kodeUser);
		userModel.setUsername(username);
		userModel.setPassword(password);

		// save join table
		userModel.setKodeRole(kodeRole);
		userModel.setKodeKaryawan(kodeKaryawan);
		// save join table
		
		userModel.setEnabled(1);

		try {
			this.userService.create(userModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "user/ubah")
	public String ubahUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/edit";
		String kodeUser = request.getParameter("kodeUser");

		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("userModel", userModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("karyawanModelList", karyawanModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "user/hapus")
	public String hapusUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/delete";
		String kodeUser = request.getParameter("kodeUser");

		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("userModel", userModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("karyawanModelList", karyawanModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "user/detail")
	public String detailUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/detail";
		String kodeUser = request.getParameter("kodeUser");

		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("userModel", userModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("karyawanModelList", karyawanModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "hasil_user")
	public String hasilUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/hasil_user";

		String kodeUser = request.getParameter("kodeUser");
		String roleUser = request.getParameter("roleUser");
		String karyawanUser = request.getParameter("karyawanUser");

		model.addAttribute("kodeUser", kodeUser);
		model.addAttribute("roleUser", roleUser);
		model.addAttribute("karyawanUser", karyawanUser);
		return halaman;
	}
	
}
