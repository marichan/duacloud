package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.MenuAksesModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.RoleModel;
import com.spring.duacloud.service.MenuAksesService;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.RoleService;

@Controller
public class MenuAksesController extends BaseController {

	@Autowired
	private MenuAksesService menuAksesService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private MenuService menuService;

	public void accessLogin(Model model) {

		// method untuk menampilkan menu by role login dan username

		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen",
				this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan",
				this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());

		// logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		// akhir method

	}

	@RequestMapping(value = "menuAkses")
	public String menuAksesMethod(Model model) {
		String halaman = "menuAkses";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "menuAkses/cari")
	public String cariMenuAksesMethod(Model model, HttpServletRequest request) {
		String halaman = "menuAkses/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<MenuAksesModel> menuAksesModelList = new ArrayList<MenuAksesModel>();

		try {
			menuAksesModelList = this.menuAksesService.cariMenuAkses(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("menuAksesModelList", menuAksesModelList);
		return halaman;
	}

	@RequestMapping(value = "menuAkses/list")
	public String listMenuAksesMethod(Model model) {
		String halaman = "menuAkses/list";
		List<MenuAksesModel> menuAksesModelList = new ArrayList<MenuAksesModel>();

		try {
			menuAksesModelList = this.menuAksesService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("menuAksesModelList", menuAksesModelList);

		return halaman;
	}

	@RequestMapping(value = "menuAkses/tambah")
	public String tambahMenuAksesMethod(Model model) {
		String halaman = "menuAkses/add";

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuService.list();
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("menuModelList", menuModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "menuAkses/simpan_delete")
	public String simpanDeleteMenuAksesMethod(HttpServletRequest request) {
		String halaman = "menuAkses";

		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		MenuAksesModel menuAksesModelDB = new MenuAksesModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			menuAksesModelDB = this.menuAksesService.cariKode(kodeMenuAkses);

			this.menuAksesService.delete(menuAksesModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menuAkses/simpan_edit")
	public String simpanEditMenuAksesMethod(HttpServletRequest request) {
		String halaman = "menuAkses";

		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		// save join table
		String kodeRole = request.getParameter("kodeRole");
		String kodeMenu = request.getParameter("kodeMenu");
		// save join table

		MenuAksesModel menuAksesModelDB = new MenuAksesModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			menuAksesModelDB = this.menuAksesService.cariKode(kodeMenuAkses);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */

			// save join table
			menuAksesModelDB.setKodeRole(kodeRole);
			menuAksesModelDB.setKodeMenu(kodeMenu);
			// save join table

			this.menuAksesService.update(menuAksesModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menuAkses/simpan_add")
	public String simpanAddMenuAksesMethod(HttpServletRequest request) {
		String halaman = "menuAkses";

		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		// save join table
		String kodeRole = request.getParameter("kodeRole");
		String kodeMenu = request.getParameter("kodeMenu");
		// save join table

		MenuAksesModel menuAksesModel = new MenuAksesModel();

		menuAksesModel.setKodeMenuAkses(kodeMenuAkses);

		// save join table
		menuAksesModel.setKodeRole(kodeRole);
		menuAksesModel.setKodeMenu(kodeMenu);
		// save join table

		try {
			this.menuAksesService.create(menuAksesModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "menuAkses/ubah")
	public String ubahMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/edit";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel", menuAksesModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("menuModelList", menuModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "menuAkses/hapus")
	public String hapusMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/delete";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel", menuAksesModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("menuModelList", menuModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "menuAkses/detail")
	public String detailMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/detail";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");

		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel", menuAksesModel);

		// skrip untuk select combobox dinamis based on data roleMsster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("roleModelList", roleModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		// skrip untuk select combobox dinamis based on data roleMsster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();

		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("menuModelList", menuModelList);
		// skrip untuk select combobox dinamis based on data roleMsster

		return halaman;
	}

	@RequestMapping(value = "hasil_menuAkses")
	public String hasilMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/hasil_menuAkses";

		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		String roleMenuAkses = request.getParameter("roleMenuAkses");
		String menuMenuAkses = request.getParameter("menuMenuAkses");

		model.addAttribute("kodeMenuAkses", kodeMenuAkses);
		model.addAttribute("roleMenuAkses", roleMenuAkses);
		model.addAttribute("menuMenuAkses", menuMenuAkses);
		return halaman;
	}

}
