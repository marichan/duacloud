package com.spring.duacloud.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class LatihanController {
	
	// method panggil jsp
	@RequestMapping(value="halaman_daftar")
	public String daftarMethod() {
		String halaman = "daftar";
		return halaman;
	}
	
	
///*	@RequestMapping(value="kategori")
//	public String kategoriMethod() {
//		String halaman = "kategori";
//		return halaman;
//	}*/
	
	/*@RequestMapping(value="item")
	public String itemMethod() {
		String halaman = "item";
		return halaman;
	}*/
	
	
	
	@RequestMapping(value="halaman_hasil_daftar")
	public String hasilDaftarMethod(HttpServletRequest request, Model model) {
		String halaman = "daftar/hasil_daftar";
		
		// get value dari name yaitu variabel html di jspnya
		// httpServletRequest request adalah function untuk get value dari jsp
		String inputStringA = request.getParameter("inputString");
		Integer inputIntegerA = Integer.valueOf(request.getParameter("inputInteger"));
		
		// set value to variabel $ di jsp hasil
		model.addAttribute("outputString", inputStringA);
		model.addAttribute("outputInteger", inputIntegerA);
		
		return halaman;
	}
	

	
//	@RequestMapping(value="hasil_kategori")
//	public String hasilKategoriMethod(HttpServletRequest request, Model model) {
//		String halaman = "kategori/hasil_kategori";
//		
//		String kodeKategori = request.getParameter("kodeKategori");
//		String namaKategori = request.getParameter("namaKategori");
//		String keterangan = request.getParameter("keterangan");
//		
//		model.addAttribute("kodeKategori", kodeKategori);
//		model.addAttribute("namaKategori", namaKategori);
//		model.addAttribute("keterangan", keterangan);
//		
//		return halaman;
//	}
	
	/*@RequestMapping(value="hasil_item")
	public String hasilItemMethod(HttpServletRequest request, Model model) {
		String halaman = "item/hasil_item";
		return halaman;
	}*/
	
	
	
	
}
