package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.PerusahaanModel;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.PerusahaanService;

@Controller
public class PerusahaanController extends BaseController{
	
	@Autowired
	private PerusahaanService perusahaanService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "perusahaan")
	public String perusahaanMethod(Model model) {
		String halaman = "perusahaan";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "perusahaan/cari")
	public String cariPerusahaanMethod(Model model, HttpServletRequest request) {
		String halaman = "perusahaan/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanModelList = this.perusahaanService.cariPerusahaan(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("perusahaanModelList", perusahaanModelList);
		return halaman;
	}

	@RequestMapping(value = "perusahaan/list")
	public String listPerusahaanMethod(Model model) {
		String halaman = "perusahaan/list";
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("perusahaanModelList", perusahaanModelList);

		return halaman;
	}

	@RequestMapping(value = "perusahaan/tambah")
	public String tambahPerusahaanMethod(Model model) {
		String halaman = "perusahaan/add";
		return halaman;
	}

	@RequestMapping(value = "perusahaan/simpan_delete")
	public String simpanDeletePerusahaanMethod(HttpServletRequest request) {
		String halaman = "perusahaan";

		String kodePerusahaan = request.getParameter("kodePerusahaan");

		PerusahaanModel perusahaanModelDB = new PerusahaanModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			perusahaanModelDB = this.perusahaanService.cariKode(kodePerusahaan);

			this.perusahaanService.delete(perusahaanModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "perusahaan/simpan_edit")
	public String simpanEditPerusahaanMethod(HttpServletRequest request) {
		String halaman = "perusahaan";

		String kodePerusahaan = request.getParameter("kodePerusahaan");
		String namaPerusahaan = request.getParameter("namaPerusahaan");
		String telpPerusahaan = request.getParameter("telpPerusahaan");

		PerusahaanModel perusahaanModelDB = new PerusahaanModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			perusahaanModelDB = this.perusahaanService.cariKode(kodePerusahaan);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			perusahaanModelDB.setNamaPerusahaan(namaPerusahaan);
			perusahaanModelDB.setTelpPerusahaan(telpPerusahaan);

			this.perusahaanService.update(perusahaanModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "perusahaan/simpan_add")
	public String simpanAddPerusahaanMethod(HttpServletRequest request) {
		String halaman = "perusahaan";

		String kodePerusahaan = request.getParameter("kodePerusahaan");
		String namaPerusahaan = request.getParameter("namaPerusahaan");
		String telpPerusahaan = request.getParameter("telpPerusahaan");

		PerusahaanModel perusahaanModel = new PerusahaanModel();

		perusahaanModel.setKodePerusahaan(kodePerusahaan);
		perusahaanModel.setNamaPerusahaan(namaPerusahaan);
		perusahaanModel.setTelpPerusahaan(telpPerusahaan);

		try {
			this.perusahaanService.create(perusahaanModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "perusahaan/ubah")
	public String ubahPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/edit";
		String kodePerusahaan = request.getParameter("kodePerusahaan");

		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel", perusahaanModel);

		return halaman;
	}

	@RequestMapping(value = "perusahaan/hapus")
	public String hapusPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/delete";
		String kodePerusahaan = request.getParameter("kodePerusahaan");

		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel", perusahaanModel);

		return halaman;
	}

	@RequestMapping(value = "perusahaan/detail")
	public String detailPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/detail";
		String kodePerusahaan = request.getParameter("kodePerusahaan");

		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel", perusahaanModel);
		return halaman;
	}

	@RequestMapping(value = "hasil_perusahaan")
	public String hasilPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/hasil_perusahaan";

		String kodePerusahaan = request.getParameter("kodePerusahaan");
		String namaPerusahaan = request.getParameter("namaPerusahaan");
		String telpPerusahaan = request.getParameter("telpPerusahaan");

		model.addAttribute("kodePerusahaan", kodePerusahaan);
		model.addAttribute("namaPerusahaan", namaPerusahaan);
		model.addAttribute("telpPerusahaan", telpPerusahaan);

		return halaman;
	}
	
}
