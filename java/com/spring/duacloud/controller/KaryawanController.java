package com.spring.duacloud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.duacloud.model.DepartemenModel;
import com.spring.duacloud.model.KaryawanModel;
import com.spring.duacloud.model.MenuModel;
import com.spring.duacloud.model.PerusahaanModel;
import com.spring.duacloud.service.DepartemenService;
import com.spring.duacloud.service.KaryawanService;
import com.spring.duacloud.service.MenuService;
import com.spring.duacloud.service.PerusahaanService;

@Controller
public class KaryawanController extends BaseController{
	
	@Autowired
	private KaryawanService karyawanService;
	
	@Autowired
	private DepartemenService departemenService;
	
	@Autowired
	private PerusahaanService perusahaanService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		model.addAttribute("namaDepartemen", this.getUserModel().getKaryawanModel().getDepartemenModel().getNamaDepartemen());
		model.addAttribute("namaPerusahaan", this.getUserModel().getKaryawanModel().getPerusahaanModel().getNamaPerusahaan());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "karyawan")
	public String karyawanMethod(Model model) {
		String halaman = "karyawan";
		this.accessLogin(model);
		return halaman;
	}

	@RequestMapping(value = "karyawan/cari")
	public String cariKaryawanMethod(Model model, HttpServletRequest request) {
		String halaman = "karyawan/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanModelList = this.karyawanService.cariKaryawan(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		model.addAttribute("karyawanModelList", karyawanModelList);
		return halaman;
	}

	@RequestMapping(value = "karyawan/list")
	public String listKaryawanMethod(Model model) {
		String halaman = "karyawan/list";
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();

		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("karyawanModelList", karyawanModelList);

		return halaman;
	}

	@RequestMapping(value = "karyawan/tambah")
	public String tambahKaryawanMethod(Model model) {
		String halaman = "karyawan/add";

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("departemenModelList", departemenModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanService.list();
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("perusahaanModelList", perusahaanModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		return halaman;
	}

	@RequestMapping(value = "karyawan/simpan_delete")
	public String simpanDeleteKaryawanMethod(HttpServletRequest request) {
		String halaman = "karyawan";

		String kodeKaryawan = request.getParameter("kodeKaryawan");

		KaryawanModel karyawanModelDB = new KaryawanModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			karyawanModelDB = this.karyawanService.cariKode(kodeKaryawan);

			this.karyawanService.delete(karyawanModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "karyawan/simpan_edit")
	public String simpanEditKaryawanMethod(HttpServletRequest request) {
		String halaman = "karyawan";

		String kodeKaryawan = request.getParameter("kodeKaryawan");
		String namaKaryawan = request.getParameter("namaKaryawan");

		// save join table
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		// save join table

		KaryawanModel karyawanModelDB = new KaryawanModel();

		try {
			/* kita cari dulu data yang lama berdasarkan kode/primarykey-nya */
			karyawanModelDB = this.karyawanService.cariKode(kodeKaryawan);

			/*
			 * kita hanya menset/mengubah nilai baru hanya untuk kolom-kolom yang diubah
			 * saja
			 */
			karyawanModelDB.setNamaKaryawan(namaKaryawan);

			// save join table
			karyawanModelDB.setKodeDepartemen(kodeDepartemen);
			karyawanModelDB.setKodePerusahaan(kodePerusahaan);
			// save join table
			
			this.karyawanService.update(karyawanModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "karyawan/simpan_add")
	public String simpanAddKaryawanMethod(HttpServletRequest request) {
		String halaman = "karyawan";

		String kodeKaryawan = request.getParameter("kodeKaryawan");
		String namaKaryawan = request.getParameter("namaKaryawan");

		// save join table
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		// save join table

		KaryawanModel karyawanModel = new KaryawanModel();

		karyawanModel.setKodeKaryawan(kodeKaryawan);
		karyawanModel.setNamaKaryawan(namaKaryawan);

		// save join table
		karyawanModel.setKodeDepartemen(kodeDepartemen);
		karyawanModel.setKodePerusahaan(kodePerusahaan);
		// save join table

		try {
			this.karyawanService.create(karyawanModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "karyawan/ubah")
	public String ubahKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/edit";
		String kodeKaryawan = request.getParameter("kodeKaryawan");

		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(kodeKaryawan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel", karyawanModel);

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("departemenModelList", departemenModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("perusahaanModelList", perusahaanModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		return halaman;
	}

	@RequestMapping(value = "karyawan/hapus")
	public String hapusKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/delete";
		String kodeKaryawan = request.getParameter("kodeKaryawan");

		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(kodeKaryawan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel", karyawanModel);

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("departemenModelList", departemenModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("perusahaanModelList", perusahaanModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		return halaman;
	}

	@RequestMapping(value = "karyawan/detail")
	public String detailKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/detail";
		String kodeKaryawan = request.getParameter("kodeKaryawan");

		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(kodeKaryawan);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel", karyawanModel);

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();

		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("departemenModelList", departemenModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		// skrip untuk select combobox dinamis based on data departemenMsster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();

		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}

		model.addAttribute("perusahaanModelList", perusahaanModelList);
		// skrip untuk select combobox dinamis based on data departemenMsster

		return halaman;
	}

	@RequestMapping(value = "hasil_karyawan")
	public String hasilKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/hasil_karyawan";

		String kodeKaryawan = request.getParameter("kodeKaryawan");
		String namaKaryawan = request.getParameter("namaKaryawan");
		String departemenKaryawan = request.getParameter("departemenKaryawan");
		String perusahaanKaryawan = request.getParameter("perusahaanKaryawan");

		model.addAttribute("kodeKaryawan", kodeKaryawan);
		model.addAttribute("namaKaryawan", namaKaryawan);
		model.addAttribute("departemenKaryawan", departemenKaryawan);
		model.addAttribute("perusahaanKaryawan", perusahaanKaryawan);
		return halaman;
	}
	
}
