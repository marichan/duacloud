<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-user-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode User :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeUser" value="${userModel.kodeUser}">
				<input type="text" name="kodeUserDisplay" id="kodeUserDisplay" class="form-input" value="${userModel.kodeUser}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Username :</label>
			<div class="col-md-6">
				<input type="text" name="username" id="username"
					class="form-input" value="${userModel.username}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Password :</label>
			<div class="col-md-6">
				<input type="password" name="password" id="password"
					class="form-input" value="${userModel.password}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Role :</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel">
						<option value="${roleModel.kodeRole}" ${roleModel.kodeRole == userModel.kodeRole ? 'selected="true"' : '' }>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Karyawan :</label>
			<div class="col-md-6">
				<select id="kodeKaryawan" name="kodeKaryawan" class="form-control">
					<c:forEach items="${karyawanModelList}" var="karyawanModel">
						<option value="${karyawanModel.kodeKaryawan}" ${karyawanModel.kodeKaryawan == userModel.kodeKaryawan ? 'selected="true"' : '' }>
							${karyawanModel.namaKaryawan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


