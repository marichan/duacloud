<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-user-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode User :</label>
			<div class="col-md-6">
				<input type="text" name="kodeUser" id="kodeUser"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Username :</label>
			<div class="col-md-6">
				<input type="text" name="username" id="username"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Password :</label>
			<div class="col-md-6">
				<input type="password" name="password" id="password"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Role :</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel">
						<option value="${roleModel.kodeRole}">
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Karyawan :</label>
			<div class="col-md-6">
				<select id="kodeKaryawan" name="kodeKaryawan" class="form-control">
					<c:forEach items="${karyawanModelList}" var="karyawanModel">
						<option value="${karyawanModel.kodeKaryawan}">
							${karyawanModel.namaKaryawan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

