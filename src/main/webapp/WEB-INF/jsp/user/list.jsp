<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${userModelList}" var="userModel">
	<tr>
		<td>${userModel.kodeUser}</td>
		<td>${userModel.username}</td>
		<td>
			<button type="button" id="btn-ubah" value="${userModel.kodeUser}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${userModel.kodeUser}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${userModel.kodeUser}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>