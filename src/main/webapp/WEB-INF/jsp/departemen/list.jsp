<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${departemenModelList}" var="departemenModel">
	<tr>
		<td>${departemenModel.kodeDepartemen}</td>
		<td>${departemenModel.namaDepartemen}</td>
		<td>${departemenModel.telpDepartemen}</td>
		<td>
			<button type="button" id="btn-ubah" value="${departemenModel.kodeDepartemen}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${departemenModel.kodeDepartemen}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${departemenModel.kodeDepartemen}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>