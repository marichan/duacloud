<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-departemen-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Departemen :</label>
			<div class="col-md-6">
				<input type="text" name="kodeDepartemen" id="kodeDepartemen"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Departemen : </label>
			<div class="col-md-6">
				<input type="text" name="namaDepartemen" id="namaDepartemen"
					class="form-input">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Departemen :</label>
			<div class="col-md-6">
				<input id="telpDepartemen" name="telpDepartemen" class="form-input">
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

