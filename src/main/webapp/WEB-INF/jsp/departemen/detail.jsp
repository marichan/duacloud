<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-departemen-detail">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Departemen :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeDepartemen" value="${departemenModel.kodeDepartemen}">
				<input type="text" name="kodeDepartemenDisplay" id="kodeDepartemenDisplay" class="form-input" value="${departemenModel.kodeDepartemen}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Departemen : </label>
			<div class="col-md-6">
				<input type="text" name="namaDepartemen" id="namaDepartemen"
					class="form-input" value="${departemenModel.namaDepartemen}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Departemen :</label>
			<div class="col-md-6">
				<input id="telpDepartemen" name="telpDepartemen" class="form-input" value="${departemenModel.telpDepartemen}" disabled="disabled">
			</div>
		</div>
	</form>
</div>
</div>

<script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-detail').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdetail.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script>


