<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-item-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Item :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeItem" value="${itemModel.kodeItem}">
				<input type="text" name="kodeItemDipslay" id="kodeItemDisplay" class="form-input" value="${itemModel.kodeItem}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Item : </label>
			<div class="col-md-6">
				<input type="text" name="namaItem" id="namaItem"
					class="form-input" value="${itemModel.namaItem}" disabled="disabled">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Kategori Item :</label>
			<div class="col-md-6">
				<select id="kodeKategori" name="kodeKategori" class="form-control" disabled="disabled">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value= "${kategoriModel.kodeKategori}" ${kategoriModel.kodeKategori == itemModel.kodeKategori ? 'selected="true"' : '' }>
							${kategoriModel.namaKategori}
						</option>				
					</c:forEach>
				</select>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Supplier Item :</label>
			<div class="col-md-6">
				<select id="kodeSupplier" name="kodeSupplier" class="form-control" disabled="disabled">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.kodeSupplier}" ${supplierModel.kodeSupplier == itemModel.kodeSupplier ? 'selected="true"' : '' }>
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Harga Jual :</label>
			<div class="col-md-6">
				<input id="hargaJual" name="hargaJual" class="form-input" value="${itemModel.hargaJual}" disabled="disabled">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Keterangan :</label>
			<div class="col-md-6">
				<textarea id="keteranganItem" name="keteranganItem" rows="5" cols="30" class="form-input" disabled="disabled">${itemModel.keteranganItem}</textarea>
			</div>

		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


