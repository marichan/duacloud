<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-item-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Item :</label>
			<div class="col-md-6">
				<input type="text" name="kodeItem" id="kodeItem"
					class="form-control" value="${kodeItemGenerator}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Item : </label>
			<div class="col-md-6">
				<input type="text" name="namaItem" id="namaItem"
					class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Kategori Item :</label>
			<div class="col-md-6">
				<select id="idKategori" name="idKategori" class="form-control">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value="${kategoriModel.idKategori}">
							${kategoriModel.namaKategori}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Supplier Item :</label>
			<div class="col-md-6">
				<select id="idSupplier" name="idSupplier" class="form-control">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.idSupplier}">
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Harga Jual :</label>
			<div class="col-md-6">
				<input id="hargaJual" name="hargaJual" class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Keterangan :</label>
			<div class="col-md-6">
				<textarea id="keteranganItem" name="keteranganItem" rows="5" cols="30" class="form-input" ></textarea>
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

