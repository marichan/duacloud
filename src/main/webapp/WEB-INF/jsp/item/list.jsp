<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${itemModelList}" var="itemModel">
	<tr>
		<td>${itemModel.kodeItem}</td>
		<td>${itemModel.namaItem}</td>
		<td>${itemModel.kategoriModel.namaKategori}</td>
		<td>
			<button type="button" id="btn-ubah" value="${itemModel.kodeItem}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${itemModel.kodeItem}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${itemModel.kodeItem}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>