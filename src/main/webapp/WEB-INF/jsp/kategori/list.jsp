<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<!-- item itu dari controller model.addAttribute -->
<!-- sedangkan var itu dari isi itemsnya -->
<c:forEach items="${kategoriModelList}" var="kategoriModel">
	<tr>
		<td>${kategoriModel.kodeKategori}</td>
		<td>${kategoriModel.namaKategori}</td>
		<td>
			<button type="button" id="btn-ubah" value="${kategoriModel.kodeKategori}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${kategoriModel.kodeKategori}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${kategoriModel.kodeKategori}" class="btn btn-danger">Delete</button>
			
		</td>
	</tr>
</c:forEach>