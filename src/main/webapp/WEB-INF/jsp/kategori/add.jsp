<div class="form-horizontal">
	<form action="#" method="get" id="form-kategori-add">
		<div class="form-group">
				<label class="control-label col-md-3">Kode Kategori :</label>
				<div class="col-md-6"><input type="text" name="kodeKategori" id="kodeKategori" 
				class="form-control" value="${kodeKategoriGenerator}"></div>
		</div>
		<div class="form-group">
				<label class="control-label col-md-3">Nama Kategori : </label>
				<div class="col-md-6"><input type="text" name="namaKategori" id="namaKategori" class="form-input"></div>
				
		</div>
		<div class="form-group">
				<label class="control-label col-md-3">Keterangan :</label>
				<div class="col-md-6"><textarea id="keteranganKategori" name="keteranganKategori" rows="5" cols="30" class="form-input"></textarea></div>
				
		</div>
		<div class="modal-footer">
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Simpan</button></div>
		</div>
	</form>
</div>

