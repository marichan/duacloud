<% request.setAttribute("contextName", request.getContextPath()); %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Halaman hasil Kategori</h1>
	<br/>
	Kode Kategori : ${kodeKategori}<br/>
	Nama Kategori : ${namaKategori}<br/>
	Keterangan : ${keteranganKategori}<br/>
	
	<a href="${contextName}/kategori.html">Kembali</a>
</body>
</html>