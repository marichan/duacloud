<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-kategori-delete">
		<div class="form-group">
				<label class="control-label col-md-3">Kode Kategori :</label>
				<div class="col-md-6">
				<input type="hidden" name="kodeKategori" value="${kategoriModel.kodeKategori}">
				<input type="text" name="kodeKategoriDisplay" id="kodeKategoriDisplay" class="form-input" value="${kategoriModel.kodeKategori}" disabled="disabled"></div>
		</div>
		<div class="form-group">
				<label class="control-label col-md-3">Nama Kategori : </label>
				<div class="col-md-6"><input type="text" name="namaKategori" id="namaKategori" class="form-input" value="${kategoriModel.namaKategori}" disabled="disabled"></div>
				
		</div>
		<div class="form-group">
				<label class="control-label col-md-3">Keterangan :</label>
				<div class="col-md-6"><textarea id="keteranganKategori" name="keteranganKategori" rows="5" cols="30" class="form-input" disabled="disabled">${kategoriModel.keteranganKategori}</textarea></div>
				
		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
	</div>
	
</div>

<script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script>


