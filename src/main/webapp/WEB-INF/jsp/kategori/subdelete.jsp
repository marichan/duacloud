<div>
	<h1>Halaman Sub-Delete</h1>
	<br />
	<button type="button" id="btn-keluar" class="btn btn-danger bottom-right">Keluar</button>
</div>

<script>
	$(document).ready(function() {
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-keluar').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/hapus.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script>