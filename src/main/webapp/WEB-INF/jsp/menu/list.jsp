<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${menuModelList}" var="menuModel">
	<tr>
		<td>${menuModel.kodeMenu}</td>
		<td>${menuModel.namaMenu}</td>
		<td>${menuModel.controllerMenu}</td>
		<td>
			<button type="button" id="btn-ubah" value="${menuModel.kodeMenu}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${menuModel.kodeMenu}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${menuModel.kodeMenu}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>