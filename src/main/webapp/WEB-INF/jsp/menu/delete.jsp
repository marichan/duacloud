<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-menu-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Menu :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeMenu" value="${menuModel.kodeMenu}">
				<input type="text" name="kodeMenuDisplay" id="kodeMenuDisplay" class="form-input" value="${menuModel.kodeMenu}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Menu : </label>
			<div class="col-md-6">
				<input type="text" name="namaMenu" id="namaMenu"
					class="form-input" value="${menuModel.namaMenu}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Menu :</label>
			<div class="col-md-6">
				<input id="controllerMenu" name="controllerMenu" class="form-input" value="${menuModel.controllerMenu}" disabled="disabled">
			</div>
		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


