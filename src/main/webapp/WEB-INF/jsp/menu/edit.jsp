<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-menu-edit">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Menu :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeMenu" value="${menuModel.kodeMenu}">
				<input type="text" name="kodeMenuDisplay" id="kodeMenuDisplay" class="form-input" value="${menuModel.kodeMenu}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Menu : </label>
			<div class="col-md-6">
				<input type="text" name="namaMenu" id="namaMenu"
					class="form-input" value="${menuModel.namaMenu}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Menu :</label>
			<div class="col-md-6">
				<input id="controllerMenu" name="controllerMenu" class="form-input" value="${menuModel.controllerMenu}">
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Simpan</button></div>
		</div>
	</form>
</div>
</div>


