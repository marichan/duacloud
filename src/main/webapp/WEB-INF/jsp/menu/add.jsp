<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-menu-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Menu :</label>
			<div class="col-md-6">
				<input type="text" name="kodeMenu" id="kodeMenu"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Menu : </label>
			<div class="col-md-6">
				<input type="text" name="namaMenu" id="namaMenu"
					class="form-input">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Controller Menu :</label>
			<div class="col-md-6">
				<input id="controllerMenu" name="controllerMenu" class="form-input">
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

