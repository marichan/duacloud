
<%
	request.setAttribute("contextName", request.getContextPath());
%>
<div>
	<h1>Halaman Daftar</h1>
	Selamat Datang di halaman Daftar 
	<a	href="${contextName}/halaman_hasil_daftar.html">Hasil Daftar</a> <br />

	<form action="halaman_hasil_daftar.html" method="get" id="form-daftar">
		Input String <input type="text" name="inputString" id="inputString"><br />
		<a id="a_inputString" style="color: red; display: none;">Input
			String Kosong !</a> Input Integer <input type="text" name="inputInteger"
			id="inputInteger"><br /> <a id="a_inputInteger"
			style="color: red; display: none;">Input Integer Kosong !</a>
		<button type="submit" value="validasi" onclick="return validasi();">Hasil</button>
	</form>

</div>

<!-- Part JavaScriptnya -->
<script type="text/javascript">
	function validasi() {
		var inputString = document.getElementById('inputString');
		var inputInteger = document.getElementById('inputInteger');
		
		var hasil = true;
		if (inputString.value == "") {
			inputString.style.borderColor = "red";
			alert('inputString Kosong');
			hasil = false;
		} else {
			inputString.style.borderColor = "none";
		}
				
		if (inputInteger.value == "") {
			inputInteger.style.borderColor = "red";
			alert('inputInteger Kosong');
			hasil = false;
		} else {
			
			// cek kembali apakah isinya numeric atau tidak
			if (isNaN(inputInteger.value)) {
				// isNaN berarti apakah nilainya "Not a Number"
				inputInteger.style.borderColor = "red";
				alert('inputInteger Harus Hanya berisi Angka');
				hasil = false;
			} else {
				// else artinya jika nilainya angka
				inputInteger.style.borderColor = "none";
			}
		}
		
		return hasil;
	}
</script>
