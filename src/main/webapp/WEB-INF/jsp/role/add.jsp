<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-role-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Role :</label>
			<div class="col-md-6">
				<input type="text" name="kodeRole" id="kodeRole"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Role : </label>
			<div class="col-md-6">
				<input type="text" name="namaRole" id="namaRole"
					class="form-input">
			</div>
		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

