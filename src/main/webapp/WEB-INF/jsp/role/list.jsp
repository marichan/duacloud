<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${roleModelList}" var="roleModel">
	<tr>
		<td>${roleModel.kodeRole}</td>
		<td>${roleModel.namaRole}</td>
		<td>
			<button type="button" id="btn-ubah" value="${roleModel.kodeRole}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${roleModel.kodeRole}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${roleModel.kodeRole}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>