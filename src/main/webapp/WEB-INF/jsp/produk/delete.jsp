<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
		<form action="#" method="get" id="form-produk-delete">
			<div class="form-group">
				<label class="control-label col-md-3">Kode Produk :</label>
				<div class="col-md-6">
					<input type="hidden" name="kodeProduk"
						value="${produkModel.kodeProduk}"> <input type="text"
						name="kodeProdukDipslay" id="kodeProdukDisplay" class="form-input"
						value="${produkModel.kodeProduk}" disabled="disabled">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nama Produk : </label>
				<div class="col-md-6">
					<input type="text" name="namaProduk" id="namaProduk"
						class="form-input" value="${produkModel.namaProduk}" disabled="disabled">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Kategori Item :</label>
				<div class="col-md-6">
					<select id="kodeKategori" name="kodeKategori" class="form-control" disabled="disabled">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value= "${kategoriModel.kodeKategori}" ${kategoriModel.kodeKategori == produkModel.kodeKategori ? 'selected="true"' : '' }>
							${kategoriModel.namaKategori}
						</option>				
					</c:forEach>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Supplier Item :</label>
				<div class="col-md-6">
					<select id="kodeSupplier" name="kodeSupplier" class="form-control" disabled="disabled">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.kodeSupplier}" ${supplierModel.kodeSupplier == produkModel.kodeSupplier ? 'selected="true"' : '' }>
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Harga Produk :</label>
				<div class="col-md-6">
					<input type="text" name="hargaProduk" id="hargaProduk"
						class="form-input" value="${produkModel.hargaProduk}" disabled="disabled">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Biaya Produk :</label>
				<div class="col-md-6">
					<input id="biayaProduk" name="biayaProduk" class="form-input"
						value="${produkModel.biayaProduk}" disabled="disabled">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Harga Total :</label>
				<div class="col-md-6">
					<input id="hargaTotal" name="hargaTotal" class="form-input"
						value="${produkModel.hargaTotal}" disabled="disabled">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Keterangan :</label>
				<div class="col-md-6">
					<textarea id="keteranganProduk" name="keteranganProduk" rows="5" disabled="disabled"
						cols="30" class="form-input">
					${produkModel.keteranganProduk} 
				</textarea>
				</div>
			</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
		</form>
	</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


