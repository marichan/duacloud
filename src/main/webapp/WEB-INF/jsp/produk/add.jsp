<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-produk-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Produk :</label>
			<div class="col-md-6">
				<input type="text" name="kodeProduk" id="kodeProduk" class="form-control" value="${kodeProdukGenerator}">
				<a id="a_kodeProduk" style="color: red; border: red; display: none;">Kode Produk Kosong !</a>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Produk : </label>
			<div class="col-md-6">
				<input type="text" name="namaProduk" id="namaProduk" class="form-input">
				<a id="a_namaProduk" style="color: red; border: red; display: none;">Nama Produk Kosong !</a>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Kategori Item :</label>
			<div class="col-md-6">
				<select id="idKategori" name="idKategori" class="form-control">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value="${kategoriModel.idKategori}">
							${kategoriModel.namaKategori}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Supplier Item :</label>
			<div class="col-md-6">
				<select id="idSupplier" name="idSupplier" class="form-control">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.idSupplier}">
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Harga Produk :</label>
			<div class="col-md-6">
				<input type="text" name="hargaProduk" id="hargaProduk" class="form-input" onkeyup="jumlahTotal();">
				<a id="a_hargaProduk" style="color: red; border: red; display: none;">Harga Produk Kosong !</a>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Biaya Produk :</label>
			<div class="col-md-6">
				<input type="text" id="biayaProduk" name="biayaProduk" class="form-input" onkeyup="jumlahTotal();">
				<a id="a_biayaProduk" style="color: red; border: red; display: none;">Biaya Produk Kosong !</a>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Harga Total :</label>
			<div class="col-md-6">
				<input type="text" id="hargaTotal" name="hargaTotal" class="form-input">
				<a id="a_hargaTotal" style="color: red; border: red; display: none;">Harga Total Kosong !</a>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Keterangan :</label>
			<div class="col-md-6">
				<textarea id="keteranganProduk" name="keteranganProduk" rows="5" cols="30" class="form-input" ></textarea>
				<a id="a_keteranganProduk" style="color: red; border: red; display: none;">Keterangan Produk Kosong !</a>
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right" onclick="cekKosong();">Simpan</button>
			</div>
		</div>
	</form>
</div>

<script>
	function cekKosong(){
		var kodeProduk = document.getElementById('kodeProduk');
		var namaProduk = document.getElementById('namaProduk');
		var hargaProduk = document.getElementById('hargaProduk');
		var biayaProduk = document.getElementById('biayaProduk');
		var hargaTotal = document.getElementById('hargaTotal');
		var keteranganProduk = document.getElementById('keteranganProduk');
		
		if (kodeProduk.value == "") {
			kodeProduk.style.boderColor = "red";
			a_kodeProduk.style.display = "block";
		}
		if (namaProduk.value == "") {
			namaProduk.style.boderColor = "red";
			a_namaProduk.style.display = "block";
		}
		if (hargaProduk.value == "") {
			hargaProduk.style.boderColor = "red";
			a_hargaProduk.style.display = "block";
		}
		if (biayaProduk.value == "") {
			biayaProduk.style.boderColor = "red";
			a_biayaProduk.style.display = "block";
		}
		if (hargaTotal.value == "") {
			hargaTotal.style.boderColor = "red";
			a_hargaTotal.style.display = "block";
		}
		if (keteranganProduk.value == "") {
			keteranganProduk.style.boderColor = "red";
			a_keteranganProduk.style.display = "block";
		} 
	}

	function jumlahTotal() {
			var hargaProduk = Number (document.getElementById('hargaProduk').value);
			var biayaProduk = Number (document.getElementById('biayaProduk').value);
			document.getElementById('hargaTotal').value= Number (hargaProduk+biayaProduk);
	}
</script>
