<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${produkModelList}" var="produkModel">
	<tr>
		<td>${produkModel.kodeProduk}</td>
		<td>${produkModel.namaProduk}</td>
		<td>${produkModel.hargaTotal}</td>
		<td>
			<button type="button" id="btn-ubah" value="${produkModel.kodeProduk}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${produkModel.kodeProduk}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${produkModel.kodeProduk}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>