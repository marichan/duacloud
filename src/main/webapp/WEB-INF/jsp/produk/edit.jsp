<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
		<form action="#" method="get" id="form-produk-edit">
			<div class="form-group">
				<label class="control-label col-md-3">Kode Produk :</label>
				<div class="col-md-6">
					<input type="hidden" name="kodeProduk" id="kodeProduk" value="${produkModel.kodeProduk}"> 
					<input type="text" name="kodeProdukDipslay" id="kodeProdukDisplay" class="form-input" value="${produkModel.kodeProduk}" disabled="disabled">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nama Produk : </label>
				<div class="col-md-6">
					<input type="text" name="namaProduk" id="namaProduk"
						class="form-input" value="${produkModel.namaProduk}">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Kategori Item :</label>
				<div class="col-md-6">
					<select id="kodeKategori" name="kodeKategori" class="form-control">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value= "${kategoriModel.kodeKategori}" ${kategoriModel.kodeKategori == produkModel.kodeKategori ? 'selected="true"' : '' }>
							${kategoriModel.namaKategori}
						</option>				
					</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Supplier Item :</label>
				<div class="col-md-6">
					<select id="kodeSupplier" name="kodeSupplier" class="form-control">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.kodeSupplier}" ${supplierModel.kodeSupplier == produkModel.kodeSupplier ? 'selected="true"' : '' }>
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Harga Produk :</label>
				<div class="col-md-6">
					<input type="text" name="hargaProduk" id="hargaProduk"
						class="form-input" value="${produkModel.hargaProduk}">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Biaya Produk :</label>
				<div class="col-md-6">
					<input id="biayaProduk" name="biayaProduk" class="form-input"
						value="${produkModel.biayaProduk}">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Harga Total :</label>
				<div class="col-md-6">
					<input id="hargaTotal" name="hargaTotal" class="form-input"
						value="${produkModel.hargaTotal}">
				</div>

			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Keterangan :</label>
				<div class="col-md-6">
					<textarea id="keteranganProduk" name="keteranganProduk" rows="5"
						cols="30" class="form-input">
						${produkModel.keteranganProduk}
					</textarea>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button type="submit" class="btn btn-danger pull-right">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	function cekKosong(){
		var kodeProduk = document.getElementById('kodeProduk');
		var namaProduk = document.getElementById('namaProduk');
		var hargaProduk = document.getElementById('hargaProduk');
		var biayaProduk = document.getElementById('biayaProduk');
		var hargaTotal = document.getElementById('hargaTotal');
		var keteranganProduk = document.getElementById('keteranganProduk');
		
		if (kodeProduk.value == "") {
			kodeProduk.style.boderColor = "red";
			a_kodeProduk.style.display = "block";
		}
		if (namaProduk.value == "") {
			namaProduk.style.boderColor = "red";
			a_namaProduk.style.display = "block";
		}
		if (hargaProduk.value == "") {
			hargaProduk.style.boderColor = "red";
			a_hargaProduk.style.display = "block";
		}
		if (biayaProduk.value == "") {
			biayaProduk.style.boderColor = "red";
			a_biayaProduk.style.display = "block";
		}
		if (hargaTotal.value == "") {
			hargaTotal.style.boderColor = "red";
			a_hargaTotal.style.display = "block";
		}
		if (keteranganProduk.value == "") {
			keteranganProduk.style.boderColor = "red";
			a_keteranganProduk.style.display = "block";
		} 
	}
</script>


