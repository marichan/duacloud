<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-karyawan-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Karyawan :</label>
			<div class="col-md-6">
				<input type="text" name="kodeKaryawan" id="kodeKaryawan"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Karyawan : </label>
			<div class="col-md-6">
				<input type="text" name="namaKaryawan" id="namaKaryawan"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Departemen Karyawan :</label>
			<div class="col-md-6">
				<select id="kodeDepartemen" name="kodeDepartemen" class="form-control">
					<c:forEach items="${departemenModelList}" var="departemenModel">
						<option value="${departemenModel.kodeDepartemen}">
							${departemenModel.namaDepartemen}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Perusahaan Karyawan :</label>
			<div class="col-md-6">
				<select id="kodePerusahaan" name="kodePerusahaan" class="form-control">
					<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
						<option value="${perusahaanModel.kodePerusahaan}">
							${perusahaanModel.namaPerusahaan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

