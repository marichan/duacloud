<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${karyawanModelList}" var="karyawanModel">
	<tr>
		<td>${karyawanModel.kodeKaryawan}</td>
		<td>${karyawanModel.namaKaryawan}</td>
		<td>${karyawanModel.departemenModel.namaDepartemen}</td>
		<td>${karyawanModel.perusahaanModel.namaPerusahaan}</td>
		<td>
			<button type="button" id="btn-ubah" value="${karyawanModel.kodeKaryawan}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${karyawanModel.kodeKaryawan}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${karyawanModel.kodeKaryawan}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>