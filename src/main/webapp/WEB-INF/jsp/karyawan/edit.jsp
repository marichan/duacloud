<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-karyawan-edit">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Karyawan :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeKaryawan" value="${karyawanModel.kodeKaryawan}">
				<input type="text" name="kodeKaryawanDisplay" id="kodeKaryawanDisplay" class="form-input" value="${karyawanModel.kodeKaryawan}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Karyawan : </label>
			<div class="col-md-6">
				<input type="text" name="namaKaryawan" id="namaKaryawan"
					class="form-input" value="${karyawanModel.namaKaryawan}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Departemen Karyawan :</label>
			<div class="col-md-6">
				<select id="kodeDepartemen" name="kodeDepartemen" class="form-control">
					<c:forEach items="${departemenModelList}" var="departemenModel">
						<option value="${departemenModel.kodeDepartemen}" ${departemenModel.kodeDepartemen == karyawanModel.kodeDepartemen ? 'selected="true"' : '' }>
							${departemenModel.namaDepartemen}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Perusahaan Karyawan :</label>
			<div class="col-md-6">
				<select id="kodePerusahaan" name="kodePerusahaan" class="form-control">
					<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
						<option value="${perusahaanModel.kodePerusahaan}" ${perusahaanModel.kodePerusahaan == karyawanModel.kodePerusahaan ? 'selected="true"' : '' }>
							${perusahaanModel.namaPerusahaan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Simpan</button></div>
		</div>
	</form>
</div>
</div>


