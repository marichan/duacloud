<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-karyawan-detail">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Karyawan :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeKaryawan" value="${karyawanModel.kodeKaryawan}">
				<input type="text" name="kodeKaryawanDisplay" id="kodeKaryawanDisplay" class="form-input" value="${karyawanModel.kodeKaryawan}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Karyawan : </label>
			<div class="col-md-6">
				<input type="text" name="namaKaryawan" id="namaKaryawan"
					class="form-input" value="${karyawanModel.namaKaryawan}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Departemen Karyawan :</label>
			<div class="col-md-6">
				<select id="kodeDepartemen" name="kodeDepartemen" class="form-control">
					<c:forEach items="${departemenModelList}" var="departemenModel">
						<option value="${departemenModel.kodeDepartemen}" ${departemenModel.kodeDepartemen == karyawanModel.kodeDepartemen ? 'selected="true"' : '' }>
							${departemenModel.namaDepartemen}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Perusahaan Karyawan :</label>
			<div class="col-md-6">
				<select id="kodePerusahaan" name="kodePerusahaan" class="form-control">
					<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
						<option value="${perusahaanModel.kodePerusahaan}" ${perusahaanModel.kodePerusahaan == karyawanModel.kodePerusahaan ? 'selected="true"' : '' }>
							${perusahaanModel.namaPerusahaan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</form>
</div>
</div>

<script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-detail').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdetail.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script>


