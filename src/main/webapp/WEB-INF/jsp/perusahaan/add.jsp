<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-perusahaan-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Perusahaan :</label>
			<div class="col-md-6">
				<input type="text" name="kodePerusahaan" id="kodePerusahaan"
					class="form-input">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Perusahaan : </label>
			<div class="col-md-6">
				<input type="text" name="namaPerusahaan" id="namaPerusahaan"
					class="form-input">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Perusahaan :</label>
			<div class="col-md-6">
				<input id="telpPerusahaan" name="telpPerusahaan" class="form-input">
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

