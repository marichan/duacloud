<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-perusahaan-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Perusahaan :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodePerusahaan" value="${perusahaanModel.kodePerusahaan}">
				<input type="text" name="kodePerusahaanDisplay" id="kodePerusahaanDisplay" class="form-input" value="${perusahaanModel.kodePerusahaan}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Perusahaan : </label>
			<div class="col-md-6">
				<input type="text" name="namaPerusahaan" id="namaPerusahaan"
					class="form-input" value="${perusahaanModel.namaPerusahaan}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">No.Telepon Perusahaan :</label>
			<div class="col-md-6">
				<input id="telpPerusahaan" name="telpPerusahaan" class="form-input" value="${perusahaanModel.telpPerusahaan}" disabled="disabled">
			</div>
		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


