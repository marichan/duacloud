<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
	<tr>
		<td>${perusahaanModel.kodePerusahaan}</td>
		<td>${perusahaanModel.namaPerusahaan}</td>
		<td>${perusahaanModel.telpPerusahaan}</td>
		<td>
			<button type="button" id="btn-ubah" value="${perusahaanModel.kodePerusahaan}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${perusahaanModel.kodePerusahaan}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${perusahaanModel.kodePerusahaan}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>