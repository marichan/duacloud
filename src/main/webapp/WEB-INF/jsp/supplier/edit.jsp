<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-supplier-edit">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeSupplier" value="${supplierModel.kodeSupplier}">
				<input type="text" name="kodeSupplierDipslay" id="kodeSupplierDisplay" class="form-input" value="${supplierModel.kodeSupplier}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier : </label>
			<div class="col-md-6">
				<input type="text" name="namaSupplier" id="namaKategori"
					class="form-input" value="${supplierModel.namaSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tipe :</label>
			<div class="col-md-6">
				<select id="tipeSupplier" name="tipeSupplier" class="form-input" value="${supplierModel.tipeSupplier}">
					<option value="1" ${supplierModel.tipeSupplier =='1'? 'selected="true"' : '' }>
						PT-Perusahaan Terbatas
					</option>
					<option value="2" ${supplierModel.tipeSupplier =='2'? 'selected="true"' : '' }>
						CV- Perusahaan Menengah
					</option>
					<option value="3" ${supplierModel.tipeSupplier =='3'? 'selected="true"' : '' }>
						Koperasi
					</option>
					<option value="4" ${supplierModel.tipeSupplier =='4'? 'selected="true"' : '' }>
						UMKM
					</option>
					<option value="5" ${supplierModel.tipeSupplier =='5'? 'selected="true"' : '' }>
						Perusahaan Individu
					</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier :</label>
			<div class="col-md-6">
				<input id="picSupplier" name="picSupplier" class="form-input" value="${supplierModel.picSupplier}">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Supplier :</label>
			<div class="col-md-6">
				<input id="telpSupplier" name="telpSupplier" class="form-input" value="${supplierModel.telpSupplier}">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Email :</label>
			<div class="col-md-6">
				<input id="emailSupplier" name="emailSupplier" class="form-input" value="${supplierModel.emailSupplier}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Alamat :</label>
			<div class="col-md-6">
				<textarea id="alamatSupplier" name="alamatSupplier" rows="5"
					cols="30" class="form-input">${supplierModel.alamatSupplier}</textarea>
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Simpan</button></div>
		</div>
	</form>
</div>
</div>

<script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-edit').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subedit.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script>



