<div class="form-horizontal">
	<form action="#" method="get" id="form-supplier-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier :</label>
			<div class="col-md-6">
				<input type="text" name="kodeSupplier" id="kodeKategori"
					class="form-control" value="${kodeSupplierGenerator}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier : </label>
			<div class="col-md-6">
				<input type="text" name="namaSupplier" id="namaKategori"
					class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Tipe :</label>
			<div class="col-md-6">
				<select id="tipeSupplier" name="tipeSupplier" class="form-input">
					<option value="01">PT - Perusahaan Terbatas</option>
					<option value="02">CV - Perusahaan Menengah</option>
					<option value="03">Koperasi</option>
					<option value="04">UMKM</option>
					<option value="05">Individu</option>
				</select>
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier :</label>
			<div class="col-md-6">
				<input id="picSupplier" name="picSupplier" class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Supplier :</label>
			<div class="col-md-6">
				<input id="telpSupplier" name="telpSupplier" class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Email :</label>
			<div class="col-md-6">
				<input id="emailSupplier" name="emailSupplier" class="form-input">
			</div>

		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Alamat :</label>
			<div class="col-md-6">
				<textarea id="alamatSupplier" name="alamatSupplier" rows="5"
					cols="30" class="form-input"></textarea>
			</div>

		</div>
		<div class="modal-footer">
			<div class="col-md-12">
				<button type="submit" class="btn btn-danger pull-right">Simpan</button>
			</div>
		</div>
	</form>
</div>

