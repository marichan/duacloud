<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${supplierModelList}" var="supplierModel">
	<tr>
		<td>${supplierModel.kodeSupplier}</td>
		<td>${supplierModel.namaSupplier}</td>
		<td>${supplierModel.telpSupplier}</td>
		<td>
			<button type="button" id="btn-ubah" value="${supplierModel.kodeSupplier}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${supplierModel.kodeSupplier}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${supplierModel.kodeSupplier}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>