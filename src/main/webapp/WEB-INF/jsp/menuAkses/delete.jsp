<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman Delete</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-menuAkses-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode MenuAkses :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeMenuAkses" value="${menuAksesModel.kodeMenuAkses}">
				<input type="text" name="kodeMenuAksesDisplay" id="kodeMenuAksesDisplay" class="form-input" value="${menuAksesModel.kodeMenuAkses}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Role :</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel">
						<option value="${roleModel.kodeRole}" ${roleModel.kodeRole == menuAksesModel.kodeRole ? 'selected="true"' : '' }>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Menu :</label>
			<div class="col-md-6">
				<select id="kodeMenu" name="kodeMenu" class="form-control">
					<c:forEach items="${menuModelList}" var="menuModel" >
						<option value="${menuModel.kodeMenu}" ${menuModel.kodeMenu == menuAksesModel.kodeMenu ? 'selected="true"' : '' }>
							${menuModel.namaMenu}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			Apakah Anda Yakin Menghapus Data Ini ?
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Ya, Hapus</button></div>
		</div>
	</form>
</div>
</div>

<!-- <script>
	$(document).ready(function (){
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-sub-delete').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'kategori/detail/subdelete.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(data) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});
</script> -->


