<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${menuAksesModelList}" var="menuAksesModel">
	<tr>
		<td>${menuAksesModel.kodeMenuAkses}</td>
		<td>${menuAksesModel.roleModel.namaRole}</td>
		<td>${menuAksesModel.menuModel.namaMenu}</td>
		<td>
			<button type="button" id="btn-ubah" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-success">Edit</button>
			<button type="button" id="btn-detail" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-warning">Detail</button>
			<button type="button" id="btn-hapus" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-danger">Delete</button>		
		</td>
	</tr>
</c:forEach>