<!-- jstl c untuk looping data -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div>
	<h1>Halaman edit</h1>
	<div class="form-horizontal">
	<form action="#" method="get" id="form-menuAkses-edit">
		<div class="form-group">
			<label class="control-label col-md-3">Kode MenuAkses :</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeMenuAkses" value="${menuAksesModel.kodeMenuAkses}">
				<input type="text" name="kodeMenuAksesDisplay" id="kodeMenuAksesDisplay" class="form-input" value="${menuAksesModel.kodeMenuAkses}" disabled="disabled">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Role :</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel">
						<option value="${roleModel.kodeRole}" ${roleModel.kodeRole == menuAksesModel.kodeRole ? 'selected="true"' : '' }>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Menu :</label>
			<div class="col-md-6">
				<select id="kodeMenu" name="kodeMenu" class="form-control">
					<c:forEach items="${menuModelList}" var="menuModel" >
						<option value="${menuModel.kodeMenu}" ${menuModel.kodeMenu == menuAksesModel.kodeMenu ? 'selected="true"' : '' }>
							${menuModel.namaMenu}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<div class="col-md-12"><button type="submit" class="btn btn-danger pull-right">Simpan</button></div>
		</div>
	</form>
</div>
</div>


