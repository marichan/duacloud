
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Menu</h1>
		<br /> Selamat Datang di Halaman Menu
		<button type="button" id="btn-tambah"
			class="btn btn-primary pull-right">Tambah</button>
	</div>

	<div class="box-body">
		<div>
			<form action="#" method="get" id="form-menu-search">
				<input type="text" id= "keyword" name="keyword" class="form-input">
				<select id= "tipe" name="tipe" class="form-input">
					<option value="kodeMenu">Kode Menu</option>
					<option value="namaMenu">Nama Menu</option>
				</select>
				<button type="submit" class="btn btn-info">Cari</button>
			</form>
			
		</div>
		<table class="table" id="tbl-menu">
			<thead>
				<tr>
					<th>Kode Menu</th>
					<th>Nama Menu</th>
					<th>Controller</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-menu">

			</tbody>
		</table>
	</div>

	<!-- div yang idnya modal-input untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>Form Menu Menu</h4>
				</div>
				<div class="modal-body">
					<!-- div yag class modal-body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>

</div>

<script>
	loadDataMenu(); /* ini untuk load fungsinya saat menu.jsp dipanggil */

	/* fungsi  javaScript untuk me-load data dari tabel menu */
	function loadDataMenu() {
		/* ajax untuk load data */
		$.ajax({
			url : 'menu/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-data-menu').html(result);
			}
		});
	}

	/* fungsi jquery */
	/* document ready itu, setelah halaman siap, function dijalankan */
	$(document).ready(function() {
	
		/* fungsi jquery untuk cari data */
		$('#form-menu-search').on('submit',function() {
			var keyword = document.getElementById('keyword').value;
			var tipe = document.getElementById('tipe').value;
			
			$.ajax({
				url : 'menu/cari.html', 
				type : 'get', 
				dataType : 'html', /* tipeCari adalah var yang akan dilempar ke Controller */
				data : {keywordCari:keyword, tipeCari:tipe}, /* tipe adalah var id dari form jsp ini */
				success : function(result) {
					$('#list-data-menu').html(result);
				}
			});
			return false; 
		});
		/* akhir fungsi jquery untuk cari data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit', '#form-menu-delete',function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/simpan_delete.json', 
				type : 'get', 
				dataType : 'json', 
				data : $(this).serialize(), 
				success : function(result) {
					$("#modal-input").modal('hide'); 
					alert("Data Menu Terhapus !")
					loadDataMenu(); /* reload list */
				}
			});
			return false; 
		});
		/* akhir fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit', '#form-menu-edit',function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/simpan_edit.json', 
				type : 'get', 
				dataType : 'json', 
				data : $(this).serialize(), 
				success : function(result) {
					$("#modal-input").modal('hide'); 
					alert("Data Menu Terubah !")
					loadDataMenu(); /* reload list */
				}
			});
			return false; 
		});
		/* akhir fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit', '#form-menu-add',function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/simpan_add.json', 
				type : 'get', 
				dataType : 'json', 
				data : $(this).serialize(), 
				success : function(result) {
					$("#modal-input").modal('hide'); 
					alert("Data Menu Tersimpan !")
					loadDataMenu(); /* reload list */
				}
			});
			return false; 
		});
		/* akhir fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save data */
		$('#modal-input').on('submit', '#form-menu', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/simpan.json', /* json itu untuk kirim data ke controller */
				type : 'get', /* type itu method */
				dataType : 'json', /* dataType itu extensi akhir dari html */
				data : $(this).serialize(), /* untuk menyimpan data apa saja yg akan disimpan */
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").modal('hide'); /* hide itu untuk close pop up */
					alert("Data Menu Tersimpan !")

					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			return false; /* return false berfungsi untuk agar tidak refresh, sehingga alertnya muncul */
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk save data */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-tambah').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/tambah.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-ubah').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/ubah.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-hapus').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/hapus.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#btn-detail').on('click', function() {
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/detail.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
		
		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#list-data-menu').on('click', '#btn-ubah', function() {
			var kodeMenuBtnEdit = $(this).val(); /* artinya get value daributton edit */
			
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/ubah.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				data : {kodeMenu:kodeMenuBtnEdit},
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#list-data-menu').on('click', '#btn-hapus', function() {
			var kodeMenuBtnDelete = $(this).val();
			
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/hapus.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				data : {kodeMenu : kodeMenuBtnDelete},
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */

		/* fungsi jquery untuk pengganti onclick di javaScript */
		$('#list-data-menu').on('click', '#btn-detail', function() {
			var kodeMenuBtnDetail = $(this).val();
			/* fungsi untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url : 'menu/detail.html', /* url itu action di form */
				type : 'get', /* type itu method */
				dataType : 'html', /* dataType itu extensi akhir dari html */
				data : {kodeMenu : kodeMenuBtnDetail},
				success : function(result) {
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* akhir munculin jsp dalam bentuk pop up */
				}
			});
			/* akhir fungsi untuk pengganti action, memanggil url request di controller */
		});
		/* akhir fungsi jquery untuk pengganti onclick di javaScript */
	});

	/* akhir document ready itu, setelah halaman siap, function dijalankan */
</script>

<%-- 	<table>
	<thead>
	<h1>Halaman Menu</h1>
		Selamat Datang di halaman Menu
		<a href="${contextName}/hasil_menu.html">Hasil Menu</a>
	</thead>
	<tbody>
		<form action="hasil_menu" method="get" id="form-menu">
		<tr>
			<td>Kode Menu</td>
			<td>:</td>
			<td><input type="text" name="kodeMenu" id="kodeMenu"> <br/></td>
		</tr>
		<tr>
			<td>Nama Menu</td>
			<td>:</td>
			<td><input type="text" name="namaMenu" id="namaMenu"> <br/></td>
		</tr>
		<tr>
			<td>Tipe</td>
			<td>:</td>
			<td><select id="tipe" name="tipe">
				<option value="01">PT - Menu Terbatas</option>
				<option value="02">CV - Menu Menengah</option>
				<option value="03">Koperasi</option>
				<option value="04">UMKM</option>
				<option value="05">Individu</option>
			</select></td>
		</tr>
		<tr>
			<td>PIC Menu</td>
			<td>:</td>
			<td><input type="text" name="picMenu" id="picMenu"> <br/></td>
		</tr>
		<tr>
			<td>Telepon Menu</td>
			<td>:</td>
			<td><input type="text" name="telpMenu" id="telpMenu"> <br/></td>
		</tr>
		<tr>
			<td>Email</td>
			<td>:</td>
			<td><input type="text" name="email" id="email"> <br/></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>:</td>
			<td><textarea name="alamat" id="alamat"></textarea></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><button type="submit" onclick="return validasi();">Submit</button>
			</td>
		</tr>
		</form>
	</tbody>
</table> --%>
